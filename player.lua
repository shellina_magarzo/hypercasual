---------------------------------------------------------------------------------------------------------------
--[[ 
player.lua (module?)
    Module description and usage is too long to place here, check out FAQ.txt for full documentation
    Function and code descriptions are still here though
--]]
---------------------------------------------------------------------------------------------------------------

----- <SCENE REQUIREMENTS> -----
local composer = require( "composer" )
local globals = require( "globals" )
local physics = require("physics")
----- </SCENE REQUIREMENTS> -----

----- <LOCAL VARIABLES> -----

----- </LOCAL VARIABLES> -----

----- <INITIALIZATIONS> -----
--physics.setDrawMode( "hybrid" )
----- </INITIALIZATIONS> -----

-- Initializing Player class
local Player = { }
Player.__index = Player
local self = setmetatable({}, Player)

--> Player's properties' initial values
-- Will be copied to the instance once player:create is called
local PlayerData = {
    name = "player",
    shape = nil,
    isDead = false,
    isAttacking = false,
    canAttack = true,
    auraSize = 20, 
    auraShape = nil,
    auraTime = 300,
    auraMaxSize = 35,
    deflections = 0,
    skinIndex = 1
}

----- <LOCAL FUNCTIONS> -----
--> The collision event with the expanding collision shape
-- self = reference to the player instance
-- event = reference to the collision event
local function auraCollision( self, event )
    local other = event.other
    if other.base then
        if not other.base.isPointed then
            self.base.deflections = self.base.deflections + 1
            other.base.isPointed = true
        end

        if not other.base.isDead then
            globals.playSFX(_G.soundTable.deflect)

            timer.performWithDelay( 50, function ( )
                self.base.isAttacking = false
                self.base.canAttack = true
            end )
            
            local event = {name="aura", other=other, target=self}
            self.base.shape:dispatchEvent( event )
        end
    end

    
end
----- </LOCAL FUNCTIONS> -----

----- <PLAYER METHODS / FUNCTIONS> -----
--> Creates the deflection aura and collision box
-- parentGroup (displayGroup) = the display group the shape is inserted to
function Player:attack( parentGroup )
    if not self.canAttack then return end

    globals.playSFX(_G.soundTable.attack)

    local shape = globals.newImageRectNoDimensions(_G.playerSkinData[self.skinIndex].deflectFile)
    shape.x, shape.y = self.shape.x, self.shape.y
    shape:scale(0.55,0.55)
    parentGroup:insert( shape )

    if _G.playerSkinData[self.skinIndex].colliderShape == "box" then
        local offsetRectParams = { halfWidth=10, halfHeight=10, angle=45 }
        physics.addBody(shape, "static", {box = offsetRectParams, bounce = 0})
    else
        physics.addBody(shape, "static", {radius = 20, bounce = 0})
    end

    shape.collision = auraCollision
    shape:addEventListener( "collision" )

    if self.auraShape and self then
        self.auraShape:removeSelf( )
        self.auraShape = nil
    end

    self.auraShape = shape
    self.auraSize = 20
    self.isAttacking = true
    self.canAttack = false

    shape.name = "aura"
    shape.base = self

    -- Animates the expanding shape
    transition.to( shape, {time = self.auraTime, xScale = 0.9, yScale = 0.9})
    transition.to( shape, {time = self.auraTime, alpha = 0, transition = easing.inQuad })
end

--> If the deflection aura/pulse has been created, make it expand
-- This is done since once the physics body has been created, you cannot edit its size
-- So what this does is it removes and readds the collision box every frame to (try to) match the image animation
-- Always test if the collision expansion is close to the image expansion animation
function Player:update(  )
    if self.auraShape and self then
        if self.isAttacking then
            self.auraSize = self.auraSize + 0.7

            physics.removeBody( self.auraShape )
            self.auraShape:removeEventListener( "collision" )

            if _G.playerSkinData[self.skinIndex].colliderShape == "box" then
                local offsetRectParams = { halfWidth=self.auraSize/1.5, halfHeight=self.auraSize/1.5, angle=45 }
                physics.addBody(self.auraShape, "static", {box = offsetRectParams, bounce = 0})
            else
                physics.addBody(self.auraShape, "static", {radius = self.auraSize, bounce = 0})
            end

            self.auraShape.collision = auraCollision
            self.auraShape:addEventListener( "collision" )

            if self.auraSize >= self.auraMaxSize then
                self.auraShape:removeSelf( )
                self.auraShape = nil
                self.isAttacking = false

                timer.performWithDelay( 300, function(  )
                    self.canAttack = true
                end )
                return
            end
        else
            display.remove( self.auraShape )
            self.auraShape = nil
        end
    end
end

--> Called when the player (not the expading force field/aura/pulse) collides with an object/enemy
local function Player_Collision( self, event )
    local other = event.other

    if other.base and not other.base.isDead then
        if not self.base.isDead then
            globals.playSFX(_G.soundTable.playerDie)

            self.base.isDead = true

            local event = {name="playerDie", other=other, target=self}
            self:dispatchEvent( event )
        end

        local event = {name="playerCollision", other=other, target=self}
        self:dispatchEvent( event )
    end
end

--> Creates the player sprite and initializes its values as well as its Physics Body
-- parentGroup (displayGroup) = the display group the player image will belong to
function Player:create( parentGroup, trailGroup, skinIndex )
    -- Set player properties based on PlayerData
    for k,v in pairs( PlayerData ) do
        self[k] = v
    end

    -- Create the trail effect
    local sheetOptions = {
        width = 70,
        height = 550,
        numFrames = 25
    }
    local trailSheet = graphics.newImageSheet( "assets/img/game/player/spritesheet_Contrail.png", sheetOptions )

     -- Sequences table for trail animation
    local sequencesData = {
        {
            name = "normal",
            start = 1,
            count = 25,
            time = 1000,
            loopCount = 0
        }
    }
    local trailSprite = display.newSprite( trailSheet, sequencesData )
    trailGroup:insert( trailSprite )
    trailSprite.x, trailSprite.y = globals.points.centerX+0.5, globals.points.centerY+230
    trailSprite:setSequence( "normal" )
    trailSprite:play()


    -- Create the player image based on the given skin index
    local shape = globals.newImageRectNoDimensions(_G.playerSkinData[skinIndex].avatarFile)
    shape.x, shape.y = globals.points.centerX, globals.points.centerY
    shape:scale( 1.2, 1.2 )
    parentGroup:insert( shape )

    -- Adds the collision box
    physics.addBody(shape, "static", {radius = 15, bounce = 0, isSensor = true})

    shape.collision = Player_Collision
    shape:addEventListener( "collision" )

    self.shape = shape
    shape.base = self

    self.skinIndex = skinIndex
    self.trail = trailSprite
end

--> Call this upon exiting the scene
function Player:destroy( )
    self.shape:removeEventListener( "collision" )
end
----- <PLAYER METHODS / FUNCTIONS> -----

return self