---------------------------------------------------------------------------------------------------------------
--[[ 
blank.lua (scene)
	This is an empty lua scene used as a transition betweeen two game.lua scenes as a workaround since you
		calling composer.gotoScene("game") inside game.lua causes an error
	All this function does is call composer.gotoScene("game")
	So if you want to refresh the game scene to a new one, use composer.gotoScene("blank")
--]]
---------------------------------------------------------------------------------------------------------------
----- <SCENE REQUIREMENTS> -----
local composer = require( "composer" )
local scene = composer.newScene()
----- <SCENE REQUIREMENTS> -----

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
----- <CREATE> -----
function scene:create( event )
	
end
----- </CREATE> -----

----- <SHOW> -----
function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then

	elseif phase == "did" then
		composer.gotoScene( "game", { effect = "crossFade", time = 500 } )
	end	
end
----- </SHOW> -----
----- <HIDE> -----
function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then

	elseif phase == "did" then
		composer.removeScene("blank")
	end	
end
----- </HIDE> -----
----- <DESTROY> -----
function scene:destroy( event )
	local sceneGroup = self.view
	physics = nil
end
----- <DESTROY> -----
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene