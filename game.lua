---------------------------------------------------------------------------------------------------------------
--[[ 
game.lua (scene)
	The scene for the game itself
	Creates the backgound and scrolls it down
	Creates the partcles and scrolls them down
	Creates the player and UI
	Spawns the enemies
	Transitions the backgrounds upon reaching the next level
	Call the update functions of the enemies and player
	Destroys objects that are off screen
	Handles the popup when player dies
	Crossfades the music when transitioning to the next level
--]]
---------------------------------------------------------------------------------------------------------------
----- <SCENE REQUIREMENTS> -----
local composer = require( "composer" )
local globals = require( "globals" )
local scene = composer.newScene()
local physics = require("physics")
local spawn = require("spawn")
local displayGroups = {}
----- <SCENE REQUIREMENTS> -----

----- <OBJECTS> -----
local player = require( "player" )

-- Arrays of the objects in the game
local backgrounds = {}
local spawnTable = {}
local particles = {}

-- Display groups
local collideGroup, particleGroup, transitionGroup, popupGroup

-- Timers
local spawnLoopTimer, particlesTimer

local distanceText -- Text that shows the distance travelled
local bgTransition -- The transition image. Is nil if there is no transition happening yet

-- Buttons
local pauseBtn
----- </OBJECTS> -----

----- <VARIABLES> -----
local curSkin = _G.save.loadData().currentSkin -- The index of the current skin the player is wearing

local scrollY = 2 -- The scroll speed (affects distance travelled)

local waveCount = 0 -- How many waves has passed
local distanceTravelled = 0 -- How much distance the player has travelled

-- The current level / background the player is on. Used in conjunction with _G.gradientList. 
-- Total number of levels is always equal to the size of gradient list table/array
local currentLevel = 1 

-- The number of bgTransition image assets
local transitionImageCount = 6

local transitionWaveCount = 5 -- How many waves before transitioning to next level
local isOnTransition = false -- Is the transition on the second loop? Used on the transition bug fix

-- Is the game currently paused? Set to true to stop the background scroll and spawning. Doesn't stop the enemies from moving though once spawned
local isGamePaused = false 

-- Has the "watch video to continue" already been used? Removes the watch to continue button once true
local isContinueUsed = false

-- Data about the spawn wave for each level
local levelSpawnData = {}

-- The current audio channel used for the background music. Should only be either 2 or 3
local curChannel = 2
----- </VARIABLES> -----

----- <INITIALIZATIONS> -----
physics.start()
physics.setGravity (0,0)

-- Produces a different sequence each time (assuming enough time between invocations)
math.randomseed( os.time() )

--> Randomizes the spawn wave each level. Add more if more spawn waves are created
for i=1, #_G.gradientList do
	local random = math.random( 3 )
	if random == 1 then
		levelSpawnData[i] = spawn.Duo
	elseif random == 2 then
		levelSpawnData[i] = spawn.Line
	elseif random == 3 then
		levelSpawnData[i] = spawn.PaperClip
	end
end
----- </INITIALIZATIONS> -----

----- <FUNCTIONS> -----
--> Creates a random transition image and sets the proper color
-- This function is a bit long and messy since it fixes the transition image bug
-- I'd rather not explain how the bug happens and how I fixed it, just hope this function fixes it for good
local function createTransition( bg )
	if isGamePaused then return end

	if not bgTransition then
		local random = math.random( transitionImageCount )
		local index = bg.index - 1
		local colorIndex = index - (2 * math.floor( index/2 ))

		display.remove( bgTransition )
		bgTransition = nil

		bgTransition = globals.newImageRectNoDimensions("assets/img/game/bgTransition" .. random .. ".png")
		bgTransition.bg = bg
		transitionGroup:insert(bgTransition)
		bgTransition.x = globals.points.centerX
		bgTransition.y = bg.y + bg.contentHeight/2 - bgTransition.contentHeight/3
		if currentLevel > 1 then
			bgTransition:setFillColor( unpack(_G.gradientList[currentLevel-1][colorIndex+1].color2) )
		else
			bgTransition:setFillColor( unpack(_G.gradientList[#_G.gradientList-1][colorIndex+1].color2) )
		end
		isOnTransition = true
	end

	if bgTransition and isOnTransition then
		local random = math.random( transitionImageCount )
		local index = bg.index - 1
		local colorIndex = index - (2 * math.floor( index/2 ))

		local before = bgTransition.bg.index - 1
		if before == 0 then before = #backgrounds end

		if bg.index == before then
			isOnTransition = false

			display.remove( bgTransition )
			bgTransition = nil

			bgTransition = globals.newImageRectNoDimensions("assets/img/game/bgTransition" .. random .. ".png")
			bgTransition.bg = bg
			transitionGroup:insert(bgTransition)
			bgTransition.x = globals.points.centerX
			bgTransition.y = bg.y + bg.contentHeight/2 - bgTransition.contentHeight/3

			if currentLevel > 1 then
				bgTransition:setFillColor( unpack(_G.gradientList[currentLevel-1][colorIndex+1].color2) )
			else
				bgTransition:setFillColor( unpack(_G.gradientList[#_G.gradientList-1][colorIndex+1].color2) )
			end
		end
	end
end


--> Update function for scrolling various objects down the screen
-- Updates the distance text as well
-- Destroys the transition image once it reaches the bottom
-- Resumes the spawn loop timer once the level is done transitioning
-- Scolls the following:
-- Background, Particles, BG Transition
local function update_scroll( event )
	if isGamePaused then return end

	distanceTravelled = distanceTravelled + scrollY/500
	distanceText.text = math.round( distanceTravelled ) .. " m"

    for i=1, #backgrounds do
    	local bg = backgrounds[i]
    	bg.y = bg.y + scrollY

    	local count = #backgrounds - 1 
    	if bg.y >= globals.points.bottom + bg.contentHeight/2 and scrollY > 0 then
    		bg.y = globals.points.centerY - (bg.contentHeight * count)
    	elseif bg.y <= globals.points.top - bg.contentHeight/2 and scrollY < 0 then
    		bg.y = globals.points.centerY + (bg.contentHeight * count)
    	end
    end

    for i=1, #backgrounds do
		local bg = backgrounds[i]
		local index = i-1
		local colorIndex = index - (2 * math.floor( index/2 ))

		if bg.y <= globals.points.top - bg.contentHeight/1.5 and bg.currentLevel ~= currentLevel then
			bg.fill = _G.gradientList[currentLevel][colorIndex+1]
			bg.currentLevel = currentLevel
			createTransition(bg)
    	end
	end

	for i=1, #particles do
		if particles[i] then
			particles[i].y = particles[i].y + particles[i].speed
		end
	end

    if bgTransition then
		bgTransition.y = bgTransition.y + scrollY

		if bgTransition.y > globals.points.top  then
			_G.musicFadeOut(curChannel, 2000)
		end

		if bgTransition.y >= globals.points.centerY and scrollY > 0 then
			isOnTransition = false

			if curChannel == 2 then
				audio.play( _G.streamTable.bgm3, {channel=3, loops=-1} )
				_G.musicFadeIn(3, 2000)
			else
				audio.play( _G.streamTable.bgm1, {channel=2, loops=-1} )
				_G.musicFadeIn(2, 2000)
			end
		end

		if bgTransition.y >= globals.points.bottom + bgTransition.contentHeight and scrollY > 0 then
			display.remove( bgTransition )
			bgTransition = nil
			timer.resume(spawnLoopTimer)

			curChannel = curChannel+1
			if curChannel > 3 then curChannel = 2 end
		end
	end
end

--> Event that fires when the screen is touched/tapped
-- Causes the player to attack/deflect
local function onScreenTouch( event )
	if isGamePaused then return end

	if ( event.phase == "began" ) then
		player:attack(collideGroup)
	end
	return true
end

--> Spawns a spawn wave depending on the levelSpawnData
-- Pauses the spawnLoopTimer and resumes it after the spawn wave delay
-- Increments the current level once transitionWaveCount has been reached
local function spawnLoop( )
	waveCount = waveCount + 1

	local spawnObj = levelSpawnData[currentLevel](collideGroup, player, waveCount, currentLevel)

	local willTransition = false

	if waveCount%transitionWaveCount == 0 and currentLevel < #_G.gradientList then
		willTransition = true
	end

	if type(spawnObj) == "table" then
		for i=1, #spawnObj do
			table.insert( spawnTable, spawnObj[i] )
		end
	else
		table.insert( spawnTable, spawnObj )
	end

	timer.pause( spawnLoopTimer )

	timer.performWithDelay( spawnObj.delay , function ( )
		if not willTransition and player and not player.isDead then
			timer.resume(spawnLoopTimer)
		end
		if willTransition then
			currentLevel = currentLevel + 1
			if currentLevel > #_G.gradientList then currentLevel = 1 end
		end
	end )
end

--> Spawns harmless particles that move downwards
-- Particles' speed and alpha are relative to its size
local function particleLoop( )
	local radius = math.random( 7 ) - math.random( )

	local particle = display.newCircle( particleGroup, math.random( globals.points.screenW ), globals.points.top - 20, radius )
	particle.alpha = (8 - radius) / 7 -- The larger the particle, the less alpha it should have
	particle.speed = (7 - radius) -- The larger the particle, the slower it should be
	table.insert( particles, particle )
end

--> Call the update functions of the player and enemies
-- Important.
local function update_objects( event )
	if isGamePaused then return end

	if player then
		player:update()
	end

	for i=1, #spawnTable do
		if spawnTable[i] then
			spawnTable[i]:update( player, particleGroup )
		end
	end
end

--> Destroys objects that have drifted off screen
local function update_destroy( event )
	-- Removes from the spawnTable objects that are somehow nil
	for i = #spawnTable, 1, -1 do
		if spawnTable[i].shape == nil then
			table.remove( spawnTable, i )
		end
	end

	for i = #spawnTable, 1, -1 do
		local shape = spawnTable[i].shape

		if (shape.base.isDead) and ( shape.x > globals.points.right + 100 or 
			shape.x < globals.points.left - 100 or 
			shape.y > globals.points.bottom + 100 or 
			shape.y < globals.points.top - 100 ) then

		  	shape.base:destroy()
			display.remove( shape )
			table.remove( spawnTable, i )

		-- If the shapes have not been deflected, but somehow wandered offscreen
		elseif ( shape.x > globals.points.right + 1000 or 
			shape.x < globals.points.left - 1000 or 
			shape.y > globals.points.bottom + 1000 or 
			shape.y < globals.points.top - 1000 ) then

			shape.base:destroy()
			display.remove( shape )
			table.remove( spawnTable, i )
		end
	end

	-- Destroyes the particles that have reached the bottom
	for i = #particles, 1, -1 do
		local particle = particles[i]
		if ( particle.y > globals.points.bottom + 50 )
	  	then
			display.remove( particle )
			table.remove( particles, i )
		end
	end
end

--> Event that fires when buttons are pressed
local function buttonRelease(event)
	local id = event.target.id

	if event.phase == "began" then
	elseif event.phase == "ended" then
		if id == "watchVideo" then
			-- Resume the timers and the game
			timer.resume(particlesTimer)
			timer.resume(spawnLoopTimer)
			
			isGamePaused = false
			isContinueUsed = true

			pauseBtn:setEnabled( true )

			-- Deletes the popup group contents once it has faded out
			transition.fadeOut( popupGroup, {time = 500, onComplete = function (  )
				for i=popupGroup.numChildren, 1, -1 do
					popupGroup[i]:removeSelf( )
				end
			end } )

			-- Revives the player
			player.isDead = false
			player.trail:play( )

			-- Destroys all enemies currently on the screen
			for i = #spawnTable, 1, -1 do
				spawnTable[i]:destroy()
				display.remove( spawnTable[i].shape )
				table.remove( spawnTable, i )
			end
			return true
		elseif id == "retry" then
			globals.playSFX(_G.soundTable.button)
			_G.musicFadeOut( curChannel, 1000 )
			composer.gotoScene( "blank", "crossFade", 0 )
			return true
		elseif id == "quit" then
			globals.playSFX(_G.soundTable.button)
			_G.musicFadeOut( curChannel, 1000 )
			composer.gotoScene( "menu", "crossFade", 500 )
			return true
		elseif id == "pause" then
			globals.playSFX(_G.soundTable.pause)
			_G.musicFadeOut( curChannel, 1000 )
			composer.gotoScene( "menu", "crossFade", 500 )
			return true
		end
	end
end

--> Event that fires whenever an enemy collides with the player's aura pulse
-- event.other = the object the player collided with (shape)
-- event.target = the reference to the hit player (shape)
local function Player_Aura( event )

end

--> Event that fires whenever the player (not the aura) collides with an enemy
--> Creates the game over popup
-- Note: Fires only once. Use the event "playerCollision" if you want the event to fire everytime a non-dead object collides with the player regarless if the player is dead or not
-- Can fire again when you set player.isDead = false
-- Important: Player will automatically set isDead = true when a non-dead enemy hits, regardless if this function is called or not

-- event.other = the object the player collided with (shape)
local function Player_Die( event )
	-- Pause game events
	timer.pause( spawnLoopTimer )
	timer.pause( particlesTimer )
	isGamePaused = true

	-- Kills enemies on the screen (but doesn't destroy/delete them)
	for i = #spawnTable, 1, -1 do
		spawnTable[i].isDead = true
		spawnTable[i].shape.isSensor = true
	end

	pauseBtn:setEnabled( false )

	-- Stops the trail animation
	player.trail:setFrame( 1 )
	player.trail:pause()

	-- Save distance score
	_G.save.saveDistanceScore( math.round( distanceTravelled ) )

	local rectOverlay = display.newRect( popupGroup, globals.points.centerX, globals.points.centerY, globals.points.screenW, globals.points.screenH)
	rectOverlay:setFillColor( 0, 0.5 )
	
	local bg = display.newRect( popupGroup, globals.points.centerX, globals.points.centerY, 250, 400 )
	bg.fill = _G.gradientList[currentLevel][1]

	_G.createBoxBorder(popupGroup, bg)
	
	local gameOver = display.newText( popupGroup, "GAME OVER", 
		globals.getLoc.centerX(bg), globals.getLoc.top(bg) + 50, _G.fontTable.Bold, 25 )
	gameOver:setFillColor( 0.2117 )

	local score = display.newText( popupGroup, math.round( distanceTravelled ) .. " m", 
		globals.getLoc.centerX(bg), globals.getLoc.top(bg) + 120, _G.fontTable.Bold, 30 )

	local bestScore = display.newText( popupGroup, "BEST: " .. _G.save.loadData().bestDistance .. " m", 
		globals.getLoc.centerX(bg), globals.getLoc.top(bg) + 190, _G.fontTable.Bold, 20 )

	local circle = globals.newImageRectNoDimensions("assets/img/menu/gameover/whiteCircle.png")
	circle.x, circle.y = score.x, score.y +5
	popupGroup:insert(circle)

	local quitBtn = globals.createImageButton( "quit",
				globals.getLoc.centerX(bg), globals.getLoc.bottom(bg) - 60,
				"assets/img/menu/gameover/btn_QuitUp.png", "assets/img/menu/gameover/btn_QuitDown.png",
				popupGroup, buttonRelease, 126, 46)

	local retryBtn = globals.createImageButton( "retry",
				globals.getLoc.centerX(bg) + 50, quitBtn.y - 80,
				"assets/img/menu/gameover/btn_RetryUp.png", "assets/img/menu/gameover/btn_RetryDown.png",
				popupGroup, buttonRelease, 85, 85)

	if not isContinueUsed then
		local watchVideoBtn = globals.createImageButton( "watchVideo",
					globals.getLoc.centerX(bg) - 50, quitBtn.y - 80,
					"assets/img/menu/gameover/btn_WatchAVideoUp.png", "assets/img/menu/gameover/btn_WatchAVideoDown.png",
					popupGroup, buttonRelease, 85, 85)
	else
		retryBtn.x = globals.getLoc.centerX(bg)
	end

	popupGroup.alpha = 0
	transition.fadeIn( popupGroup, {time = 1000} )
end

----- </FUNCTIONS> -----

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
----- <CREATE> -----
function scene:create( event )
	local sceneGroup = self.view

	-- Create the display groups
	local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
	transitionGroup = globals.createDisplayGroup( "transitionGroup", displayGroups, sceneGroup )
	local trailGroup = globals.createDisplayGroup( "collideGroup", displayGroups, sceneGroup )
	collideGroup = globals.createDisplayGroup( "collideGroup", displayGroups, sceneGroup )
	local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	particleGroup = globals.createDisplayGroup( "particleGroup", displayGroups, sceneGroup )
	local UIGroup = globals.createDisplayGroup( "UIGroup", displayGroups, sceneGroup )
	popupGroup = globals.createDisplayGroup( "popupGroup", displayGroups, sceneGroup )

	-- Creates the scrolling background and positions them
	for i=1, 4 do	
		local bg = display.newRect( backGroup, globals.points.centerX, globals.points.centerY, globals.points.screenW, globals.points.screenH )
		i = i-1
		local colorIndex = i - (2 * math.floor( i/2 ))
		bg.fill = _G.gradientList[currentLevel][colorIndex+1]
		bg.currentLevel = currentLevel

		bg.index = i+1

		local yPos = (globals.points.centerY - (bg.contentHeight*i))
		bg.y = yPos

		table.insert( backgrounds, bg )
	end

	-- Create the player and add listeners to its events
	player:create( mainGroup, trailGroup, curSkin )
    player.shape:addEventListener( "aura", Player_Aura )
    player.shape:addEventListener("playerDie", Player_Die)
    --player.shape:addEventListener("playerCollision", Player_Collision)

    local distanceTitle = display.newText( UIGroup, "DISTANCE", 
		globals.points.centerX, globals.points.top + 40, _G.fontTable.Regular, 20 )
 	distanceTitle:setFillColor( 0.2217 )

    distanceText = display.newText( UIGroup, distanceTravelled .. " m", 
		globals.points.centerX, distanceTitle.y + 20, _G.fontTable.Bold, 25 )
    distanceText:setFillColor( 0.2217 )
	

	pauseBtn = globals.createImageButton( "pause",
				globals.points.left + 20, globals.points.top + 20,
				"assets/img/game/btn_PauseUp.png", "assets/img/game/btn_PauseDown.png",
				UIGroup, buttonRelease, 20, 20)

	transition.from( pauseBtn, { time=600, y=globals.points.top - 10, alpha=0 } )
	transition.from( distanceTitle, { time=600, y=globals.points.top - 10, alpha=0 } )
	transition.from( distanceText, { time=500, y=globals.points.top - 10, alpha=0 } )

	audio.play( _G.streamTable.bgm1, {channel=curChannel, loops=-1} )
	_G.musicFadeIn(curChannel, 1000)
end
----- </CREATE> -----

----- <SHOW> -----
function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then

	elseif phase == "did" then
		physics.start()
		Runtime:addEventListener("enterFrame", update_scroll)
		Runtime:addEventListener("enterFrame", update_objects)
		Runtime:addEventListener("enterFrame", update_destroy)

		Runtime:addEventListener( "touch", onScreenTouch )

		spawnLoopTimer = timer.performWithDelay(500, spawnLoop, 0)
		particlesTimer = timer.performWithDelay( 300, particleLoop, 0 )
	end	
end
----- </SHOW> -----
----- <HIDE> -----
function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		Runtime:removeEventListener("enterFrame", update_scroll)
		Runtime:removeEventListener("enterFrame", update_objects)
		Runtime:removeEventListener("enterFrame", update_destroy)

		Runtime:removeEventListener( "touch", onScreenTouch )

		player.shape:removeEventListener( "aura", Player_Aura )
    	player.shape:removeEventListener("playerDie", Player_Die)

		timer.cancel( spawnLoopTimer )
		timer.cancel( particlesTimer )

		player:destroy()
		player = nil

	elseif phase == "did" then
		physics.pause()
		composer.removeScene("game")
	end	
end
----- </HIDE> -----
----- <DESTROY> -----
function scene:destroy( event )
	local sceneGroup = self.view
	physics = nil
end
----- <DESTROY> -----
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene