---------------------------------------------------------------------------------------------------------------
--[[ 
globals.lua (module)
    Contains various helpful global functions to make code organized and cleaner
    Some of these came from the code of Word Treats and Word Cuisine, so I can't really explain what they do, so I'll just 
explain the ones I added.

    To use:
    At the start of the lua code, add this
        local globals = require( "globals" )
    Then to use, call the functions that was assigned to this module, for example:
        globals.points.left
    It will return the X value of the left most part of the screen

    Remember to add the parameters if the function asks for it
    Add some more functions of your own if needed, just follow the syntax as the others and please please comment it for future use
    Also make sure the those functions can be used everywhere, and is transferrable to other projects
--]]
---------------------------------------------------------------------------------------------------------------

local M = {}
local widget = require( "widget" )

local function createDisplayGroup( id, displayGroups, sceneGroup )
    local displayGroup = display.newGroup()
    displayGroup.id = id
    sceneGroup:insert( displayGroup )
    table.insert( displayGroups, displayGroup )
    return displayGroup
end

local function getTableMemberWithID( id, t )
    local result = nil
    for i = 1, #t do
        local member = t[ i ]
        if member.id == id then
            result = member
        end
    end
    return result
end

local function togglePanel( id, panels, displayGroups )
    local panel = getTableMemberWithID( id, t )
    local parentGroup = getTableMemberWithID( panel.parentGroupID, displayGroups )

    local visibility = not panel.isVisible
    panel.isVisible = visibility
    for i = 1, parentGroup.numChildren do
        local panelMember = parentGroup[ i ]
        panelMember.isVisible = visibility
    end

    -- TODO: remove listeners outside of panel??
end

local function addListeners( objs )
    for i = 1, #objs do
        local obj = objs[ i ]
        if obj.hasListener then
            obj:addEventListener( obj.listenerOptions.eventName, obj.listenerOptions.listener )
        end
    end
end

local function removeListeners( objs )
    for i = 1, #objs do
        local obj = objs[ i ]
        if obj.hasListener then
            obj:removeEventListener( obj.listenerOptions.eventName, obj.listenerOptions.listener )
        end
    end
end

--> Anchors the object to its Top Left
local function setBetterAnchor( obj )
    obj.anchorX = 0
    obj.anchorY = 0
end

--> Creates a button using an image and returns it
-- id = a string to identify the button, usually used on the releaseEventListener function
-- x,y = the x and y positions of the button
-- defaultFile = the file name of the default image of the button
-- overFile = the file name of the image of the button when it is pressed
-- parentGroup = the display group this button will belong to
-- releaseEventListener - the function that will be called when this button is pressed, don't forget to put (event) as a paramter
-- width, height = the width and height of the button. Image will be scaled/stretched depending on the value
-- isOnEventListener (bool) = optional. Whether releaseEventListener will be considered an onEvent listener or an onRelease listener. Default is false
local function createImageButton( id, x, y, defaultFile, overFile, parentGroup, releaseEventListener, width, height, isOnEventListener)
    local button = nil

    if not isOnEventListener then
        button = widget.newButton
        {
            id = id,
            x = x,
            y = y,
    		width = width,
    		height = height,
            defaultFile = defaultFile,
            overFile = overFile,
            onRelease = releaseEventListener
        }
    else
        button = widget.newButton
        {
            id = id,
            x = x,
            y = y,
            width = width,
            height = height,
            defaultFile = defaultFile,
            overFile = overFile,
            onEvent = releaseEventListener
        }
    end
    parentGroup:insert( button )
    return button
end

----- *Doesn't actually change the given button's image
--> Recreates the given button but with a different parentGroup, defaultFile (upFile) and overFile (downFile) and returns it
--> It's your responsibility to remove the old button from display or from wherever
-- oldBtn (ButtonWidget) = the reference to the button you want to recreate/change
-- newUpFile (string) = the file name of the new default/upFile
-- newDownFile (string) = the file name of the new over/downFile
-- parentGroup (displayGroup) = the display group the new button will be inserted to
-- buttonEvent (function) = the function that will listen to the button's events
-- isOnEventListener (bool) = optional. Whether releaseEventListener will be considered an onEvent listener or an onRelease listener. Default is false
local function changeButtonImage( oldBtn, newUpFile, newDownFile, parentGroup, buttonEvent, isOnEventListener )
    assert( parentGroup, "No parent group!!" )
    local newBtn = createImageButton( oldBtn.id, oldBtn.x, oldBtn.y,
                    newUpFile, newDownFile, parentGroup, buttonEvent, oldBtn.contentWidth, oldBtn.contentHeight, isOnEventListener)
    return newBtn
end

--> Creates a basic horizontal slider and returns it
-- id = a string to identify the button, usually used on the listener function
-- x, y = the x and y positions of the slider
-- parent = the display group this slider will belong to
-- lister = the function that will be called when this slider is moved, etc. Don't forget to put (event) as a parameter
-- value = the initial value of the slider, 0 - 100 from left to right
-- width = the width of the slider
local function createHSlider( id, x, y, parent, listener, value, width)
    local slider = widget.newSlider(
        {
            id = id,
            x = x,
            y = y,
            value = value,  
            listener = listener,
            width = length
        }
    )
    parent:insert( slider )
    return slider
end

--> Creates an image rect with no starting dimensions given the file name then returns it
-- loc is optional
local function newImageRectNoDimensions( filename , loc)

    if loc == nil then loc = system.ResourceDirectory end
    local w, h = 0, 0
    local image
    image = display.newImage( filename, loc )
    w =  math.floor(image.width)
    h =  math.floor(image.height)
    image:removeSelf()

    return display.newImageRect( filename, loc, w, h )
end

--> Plays a random sfx/sound from the given array.
-- sfx = an array of handles to sound files. audio.loadSound() should be used
-- channel = the channel the audio will play on. Is optional, default will be 0 (channel is auto picked)
-- loops = the number of loops. Is optional, default will be 0 (the audio will only play once)
local function playSFX(sfx, channel, loops)
    local rand = math.random( #sfx )
    local thisLoops = loops or 0
    local thisChannel = channel or 0
    audio.play( sfx[rand], {channel=thisChannel, loops=thisLoops} )
end

--> Used for anchoring objects
-- To use: globals.setAnchor.Center(t) where t = the object whose anchor will be edited
local setAnchor = {
    TopLeft = function(t) t.anchorX, t.anchorY = 0, 0; end,
    TopCenter = function(t) t.anchorX, t.anchorY = .5, 0; end,
    TopRight = function(t) t.anchorX, t.anchorY = 1, 0; end,
    CenterLeft = function(t) t.anchorX, t.anchorY = 0, .5; end,
    Center = function(t) t.anchorX, t.anchorY = .5, .5; end,
    CenterRight = function(t) t.anchorX, t.anchorY = 1, .5; end,
    BottomLeft = function(t) t.anchorX, t.anchorY = 0, 1; end,
    BottomCenter = function(t) t.anchorX, t.anchorY = .5, 1; end,
    BottomRight = function(t) t.anchorX, t.anchorY = 1, 1; end
}

--> Returns useful positions in the screen
-- X values: left, right, centerX, screenW
-- Y values: top, bottom, centerY, screenH
local points = {
    top = display.screenOriginY,
    left = display.screenOriginX,
    bottom = display.contentHeight - display.screenOriginY,
    right = display.contentWidth - display.screenOriginX,
    centerX = display.contentCenterX,
    centerY = display.contentCenterY,
    screenW = display.actualContentWidth, -- screen width
    screenH = display.actualContentHeight, -- screen height
}

--> Returns useful positions of the given display object
-- X values: left, right, centerX, W
-- Y values: top, bottom, centerY, H
local getLoc = {
    top = function(t) return t.y - t.contentHeight/2 end,
    left = function(t) return t.x - t.contentWidth/2 end,
    bottom = function(t) return t.y + t.contentHeight/2 end,
    right = function(t) return t.x + t.contentWidth/2 end,
    centerX = function(t) return t.x end,
    centerY = function(t) return t.y end,
    W = function(t) return t.contentWidth end, -- The width
    H = function(t) return t.contentHeight end -- The height
}

--> Returns the lerp of a and b
-- a = the minimum value
-- b = the maximum value
-- f = the alpha value (0 - 1)
local function lerp(a, b, f)
    return a + f * (b - a);
end

--> Returns the f given the lerp, a and b
-- See above lerp function for parameter details
local function reverseLerp(lerp, a, b)
    return (lerp-a)/(b-a)
end


-- The list of functions assigned to this module
M.getTableMemberWithID = getTableMemberWithID
M.togglePanel = togglePanel
M.createDisplayGroup = createDisplayGroup
M.addListeners = addListeners
M.removeListeners = removeListeners
M.setBetterAnchor = setBetterAnchor
M.createImageButton = createImageButton
M.changeButtonImage = changeButtonImage
M.createHSlider = createHSlider
M.newImageRectNoDimensions = newImageRectNoDimensions
M.playSFX = playSFX
M.setAnchor = setAnchor
M.points = points
M.getLoc = getLoc
M.lerp = lerp
M.reverseLerp = reverseLerp

return M
