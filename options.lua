---------------------------------------------------------------------------------------------------------------
--[[ 
options.lua (overlay scene)
	Popup scene for the settings/options
	Mutes/Unmutes the sound and music
	Saves the settings in a persistent file
	No functionality yet for the following buttons: credits, facebook and more games
--]]
---------------------------------------------------------------------------------------------------------------
----- <SCENE REQUIREMENTS> -----
local composer = require( "composer" )
local globals = require( "globals" )
local scene = composer.newScene()
local displayGroups = {}
----- <SCENE REQUIREMENTS> -----

----- <OBJECTS> -----
local bg -- Background image
local soundBtn, musicBtn

local mainGroup -- Display group
----- </OBJECTS> -----

----- <VARIABLES> -----
local isSoundOn = _G.save.loadData().isSoundOn
local isMusicOn = _G.save.loadData().isMusicOn

local updateAudioButtons -- Function to update audio button images depending on the saved settings
----- </VARIABLES> -----

----- <INITIALIZATIONS> -----

----- </INITIALIZATIONS> -----

----- <FUNCTIONS> -----
local function buttonRelease(event)
	local id = event.target.id

	if event.phase == "began" then
	elseif event.phase == "ended" then
		if id == "close" then
			composer.hideOverlay("crossFade", 500 )
		elseif id == "sound" then
			isSoundOn = not isSoundOn
			_G.save.saveSoundSetting(isSoundOn)
			updateAudioButtons()
		elseif id == "music" then
			isMusicOn = not isMusicOn
			_G.save.saveMusicSetting(isMusicOn)
			updateAudioButtons()
		end
		globals.playSFX(_G.soundTable.button)
	end
end

updateAudioButtons = function(  )
	if soundBtn then soundBtn:removeSelf( ) end
	if musicBtn then musicBtn:removeSelf( ) end

	local on = "On"
	if not isSoundOn then on = "Off" end

	soundBtn = globals.createImageButton( "sound",
				globals.getLoc.centerX(bg) - 40, globals.getLoc.top(bg) + 100,
				"assets/img/menu/options/btn_Sounds" .. on .. "Up.png", "assets/img/menu/options/btn_Sounds" .. on .. "Down.png",
				mainGroup, buttonRelease, 50, 50)

	local on = "On"
	if not isMusicOn then on = "Off" end

	musicBtn = globals.createImageButton( "music",
				globals.getLoc.centerX(bg) + 40, globals.getLoc.top(bg) + 100,
				"assets/img/menu/options/btn_Music" .. on .. "Up.png", "assets/img/menu/options/btn_Music" .. on .. "Down.png",
				mainGroup, buttonRelease, 50, 50)

	_G.updateAudioPerSetting()
end
----- </FUNCTIONS> -----

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
----- <CREATE> -----
function scene:create( event )
	local sceneGroup = self.view

	local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
	mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )

	local rectOverlay = display.newRect( backGroup, globals.points.centerX, globals.points.centerY, globals.points.screenW, globals.points.screenH)
	rectOverlay:setFillColor( 0, 0.5 )

	bg = display.newRect( backGroup, globals.points.centerX, globals.points.centerY, 250, 350 )
	bg.fill = {
		type = "gradient",
	    color1 = { 0.0235,1,0.9882 },
	    color2 = { 0,0.6666,0.8588 }
	}

	_G.createBoxBorder(backGroup, bg)

	local titleText = display.newText( mainGroup, "OPTIONS", globals.getLoc.centerX(bg), globals.getLoc.top(bg) + 40, _G.fontTable.Bold, 25 )
	titleText:setFillColor( 0.2117 )

	local closeBtn = globals.createImageButton( "close",
				globals.getLoc.right(bg) - 20, globals.getLoc.top(bg) + 20,
				"assets/img/menu/options/btn_CloseUp.png", "assets/img/menu/options/btn_CloseDown.png",
				mainGroup, buttonRelease, 36, 36)

	local gamesBtn = globals.createImageButton( "moreGames",
				globals.getLoc.centerX(bg), globals.getLoc.bottom(bg) - 60,
				"assets/img/menu/options/btn_MoreGamesUp.png", "assets/img/menu/options/btn_MoreGamesDown.png",
				mainGroup, buttonRelease, 180, 46)
	local fbBtn = globals.createImageButton( "facebook",
				globals.getLoc.centerX(bg), gamesBtn.y - 55,
				"assets/img/menu/options/btn_FBLikeUp.png", "assets/img/menu/options/btn_FBLikeDown.png",
				mainGroup, buttonRelease, 126, 46)
	local creditBtn = globals.createImageButton( "credits",
				globals.getLoc.centerX(bg), fbBtn.y - 55,
				"assets/img/menu/options/btn_CreditsUp.png", "assets/img/menu/options/btn_CreditsDown.png",
				mainGroup, buttonRelease, 126, 46)

	updateAudioButtons()
end
----- </CREATE> -----

----- <SHOW> -----
function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then

	elseif phase == "did" then
	end	
end
----- </SHOW> -----
----- <HIDE> -----
function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then

	elseif phase == "did" then
		composer.removeScene("options")
	end	
end
----- </HIDE> -----
----- <DESTROY> -----
function scene:destroy( event )
	local sceneGroup = self.view
	physics = nil
end
----- <DESTROY> -----
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene