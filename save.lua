---------------------------------------------------------------------------------------------------------------
--[[ 
save.lua (module)
    Contains functionality for saving data to a persistent file and loading them
    The table that loadData() returns has the following keys:
        isEmpty (bool) = is nil or false when the data has not been initialized yet and save file hasn't been created yet
        bestDistance (number) = the highest score for distance
        currentSkin (number) = the index of the current skin the player is wearing (number should not be greater than the length of _G.playerSkinData)
        isSoundOn (bool) = false if all sound effects are muted
        isMusicOn (bool) = false if all music are muted
--]]
---------------------------------------------------------------------------------------------------------------
local M = {}

local json = require("json")
local filePath = system.pathForFile ("settings.json", system.DocumentsDirectory)

--> Returns a table of the saved data
local function loadData()
    local file = io.open (filePath, "r") -- "r" means read only
    local dataTable = {}

    if file then 
        local contents = file:read ("*a")
        io.close(file)
        dataTable = json.decode(contents)
    end -- file if end
    
    -- Initial Values for the data
    if dataTable == nil or not dataTable.isEmpty then
        dataTable.isEmpty = true
        dataTable.bestDistance = 0
        dataTable.currentSkin = 1
        dataTable.isSoundOn = true
        dataTable.isMusicOn = true
    end
    return dataTable
end

--> Saves the given dataTable table into a persistent text file
local function saveData(dataTable)
    local file = io.open(filePath, "w") -- "w" write access

    -- overwrite the file
    if file then
        file:write (json.encode (dataTable) )
        io.close(file)
    end -- if statement end
end

--> Saves the distance score if it is higher than the current score
local function saveDistanceScore( score )
    local dataTable = loadData()

    if dataTable.bestDistance < score then
        dataTable.bestDistance = score
    end

    saveData(dataTable)
end

--> Saves the given skin index
local function savePlayerSkin( skinIndex )
    local dataTable = loadData()
    dataTable.currentSkin = skinIndex
    saveData(dataTable)
end

--> Saves the given sound bool
local function saveSoundSetting( isSoundOn )
    local dataTable = loadData()
    dataTable.isSoundOn = isSoundOn
    saveData(dataTable)
end

--> Saves the given music bool
local function saveMusicSetting( isMusicOn )
    local dataTable = loadData()
    dataTable.isMusicOn = isMusicOn
    saveData(dataTable)
end

-- The list of functions assigned to this module
M.loadData = loadData
M.saveDistanceScore = saveDistanceScore
M.savePlayerSkin = savePlayerSkin
M.saveSoundSetting = saveSoundSetting
M.saveMusicSetting = saveMusicSetting

return M
