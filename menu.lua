---------------------------------------------------------------------------------------------------------------
--[[ 
menu.lua (scene)
	The scene for the main menu / title screen
	Creates the buttons to open the following scenes: options, leaderboard, shop and skins
	Plays the animation of the player avatar when the game is started
--]]
---------------------------------------------------------------------------------------------------------------

----- <SCENE REQUIREMENTS> -----
local composer = require( "composer" )
local globals = require( "globals" )
local scene = composer.newScene()
local widget = require("widget")
local displayGroups = {}
----- </SCENE REQUIREMENTS> -----

----- <OBJECTS> -----
local mainGroup -- Display group
local shape -- The image of the player avatar that acts as the play button
local playLine
local tapStartText

-- Text objects
local buildNumberText, titleText

-- Button Widgets
local optionsBtn, leaderBoardBtn, shopBtn, skinBtn
----- <OBJECTS> -----

----- <VARIABLES> -----
local curSkin = _G.save.loadData().currentSkin -- The index of the current skin the player is wearing
----- <VARIABLES> -----

----- <INITIALIZATIONS> -----

----- </INITIALIZATIONS> -----

----- <FUNCTIONS> -----
--> Animation when the player starts the game
local fadeOutTime = 500
local function fadeOutEffect(  )
	transition.to( titleText, {time = fadeOutTime, alpha = 0, y = globals.points.top + 100} )
	transition.to( tapStartText, {time = fadeOutTime, alpha = 0, y = globals.points.top + 200} )
	transition.to( playLine, {time = fadeOutTime, alpha = 0, y = globals.points.top + 250} )

	transition.to( optionsBtn, {time = fadeOutTime, alpha = 0, y = globals.points.bottom - 100} )
	transition.to( leaderBoardBtn, {time = fadeOutTime, alpha = 0, y = globals.points.bottom - 100} )
	transition.to( shopBtn, {time = fadeOutTime, alpha = 0, y = globals.points.bottom - 100} )
	transition.to( skinBtn, {time = fadeOutTime, alpha = 0, y = globals.points.bottom - 100} )

	transition.to( buildNumberText, {time = fadeOutTime, alpha = 0, y = globals.points.bottom, onComplete = function ( )
		composer.gotoScene( "game" )
	end} )
end

--> Event that is called when the player avatar/shape is tapped
local function playButton(event)
	shape:removeEventListener( "tap", playButton )

	_G.musicFadeOut( 1, 2000 )

	local deflect = globals.newImageRectNoDimensions(_G.playerSkinData[curSkin].deflectFile)
    deflect.x, deflect.y = globals.points.centerX, globals.points.centerY
    deflect:scale(0.55,0.55)
    mainGroup:insert( deflect )

    transition.to( deflect, {time = 300, xScale = 0.9, yScale = 0.9})
    transition.to( deflect, {time = 300, alpha = 0, transition = easing.inQuad, onComplete = fadeOutEffect})	
end

--> Event when buttons are clicked
local function buttonRelease(event)
	local id = event.target.id

	if event.phase == "began" then
	elseif event.phase == "ended" then
		globals.playSFX(_G.soundTable.button)
		if id == "options" then
			composer.showOverlay( "options", { effect = "fade", time = 500,  isModal = true } )  
			return true
		elseif id == "leaderBoard" then
			
			return true
		elseif id == "shop" then
			
			return true
		elseif id == "skin" then
			composer.showOverlay( "skins", { effect = "fade", time = 500,  isModal = true } )
			return true
		end
	end
end
----- <FUNCTIONS> -----


------------------------------------------------------------
-- Scene Events
------------------------------------------------------------
----- <CREATE> -----
function scene:create( event )
	local sceneGroup = self.view

	local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )

    -- Create the background
	local bg = display.newRect( backGroup, globals.points.centerX, globals.points.centerY, globals.points.screenW, globals.points.screenH )
	bg.fill = _G.gradientList[1][1]

	-- Create the player image/shape based on the current skin
	shape = globals.newImageRectNoDimensions(_G.playerSkinData[curSkin].avatarFile)
    shape.x, shape.y = globals.points.centerX, globals.points.centerY
    shape:scale( 1.2, 1.2 )
    mainGroup:insert( shape )

    -- The build number at the lower right of the screen
	buildNumberText = display.newText( mainGroup, _G.buildNumber, globals.points.right - 25, globals.points.bottom - 20, _G.fontTable.SemiBold, 15 )
	buildNumberText:setFillColor( 0.2117 )

	-- The title. Change this to an image if the title asset has been made
	titleText = display.newText( mainGroup, "Hypercasual Game", globals.points.centerX, globals.points.centerY - 170, _G.fontTable.Bold, 25 )
	titleText:setFillColor( 0.2117 )

	-- The text that says "Tap to Start"
	tapStartText = display.newText( mainGroup, "TAP TO START", globals.points.centerX, globals.points.centerY - 70, _G.fontTable.Regular, 20 )
	tapStartText:setFillColor( 0.2117 )
	
	-- The decoration line
	playLine = globals.newImageRectNoDimensions("assets/img/menu/title/tDesign.png")
	playLine.x, playLine.y = tapStartText.x, tapStartText.y + 30
	mainGroup:insert( playLine )

	-- The butttons at the bottom of the title screen
	optionsBtn = globals.createImageButton( "options",
				globals.points.centerX - 105, globals.points.centerY + 150,
				"assets/img/menu/title/btn_OptionsUp.png", "assets/img/menu/title/btn_OptionsDown.png",
				mainGroup, buttonRelease, 60, 60)

	leaderBoardBtn = globals.createImageButton( "leaderBoard",
				globals.points.centerX - 35, globals.points.centerY + 150,
				"assets/img/menu/title/btn_LeaderboardsUp.png", "assets/img/menu/title/btn_LeaderboardsDown.png",
				mainGroup, buttonRelease, 60, 60)

	shopBtn = globals.createImageButton( "shop",
				globals.points.centerX + 35, globals.points.centerY + 150,
				"assets/img/menu/title/btn_ShopUp.png", "assets/img/menu/title/btn_ShopDown.png",
				mainGroup, buttonRelease, 60, 60)

	skinBtn = globals.createImageButton( "skin",
				globals.points.centerX + 105, globals.points.centerY + 150,
				"assets/img/menu/title/btn_SkinsUp.png", "assets/img/menu/title/btn_SkinsDown.png",
				mainGroup, buttonRelease, 60, 60)

	-- Tap the player avatar to start
	shape:addEventListener( "tap", playButton )

	-- Play the title screen music
	audio.play(_G.streamTable.titleScreen, { channel=1, loops=-1} )
	_G.musicFadeIn( 1, 1000 )
end
----- </CREATE> -----

--> Called after exiting skins.lua to update the player avatar to the current skin
function scene:updateSkin(  )
	if shape then 
		shape:removeEventListener( "tap", playButton )
		shape:removeSelf( )
		shape = nil
	end
	curSkin = _G.save.loadData().currentSkin

	shape = globals.newImageRectNoDimensions(_G.playerSkinData[curSkin].avatarFile)
    shape.x, shape.y = globals.points.centerX, globals.points.centerY
    shape:scale( 1.2, 1.2 )
    mainGroup:insert( shape )

    shape:addEventListener( "tap", playButton )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then

	elseif phase == "did" then
		
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		shape:removeEventListener( "tap", playButton )
	elseif phase == "did" then
		composer.removeScene("menu")
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene