---------------------------------------------------------------------------------------------------------------
--[[ 
spawn.lua (module)
    Module description and usage is too long to place here, check out FAQ.txt for full documentation
    Function and code descriptions are still here though
--]]
---------------------------------------------------------------------------------------------------------------

----- <SCENE REQUIREMENTS> -----
local M = {}
local globals = require( "globals" )
local physics = require("physics")
----- </SCENE REQUIREMENTS> -----

----- <INITIALIZATIONS> -----
--physics.setDrawMode( "hybrid" )
physics.start()
physics.setGravity (0,0)

----- </INITIALIZATIONS> -----

----- <LOCAL FUNCTIONS> -----
--> When the projectile collides with another object
local function Projectile_Collision( self, event )
    local other = event.other

    if other.base and not self.base.isDead then
        if other.name == "aura" then
            timer.performWithDelay( 0, function (  )
                self.isSensor  = true
            end )

            self.alpha = 0.5

            timer.performWithDelay( 10, function ( )
                self.base.isDead = true
            end )
        elseif other.name ~= "player" then
            globals.playSFX(_G.soundTable.bump)
        end
    end
end

--> Have the physics object move towards the player
-- player = the reference to the player
-- shape = the reference to the enemy (shape not self)
-- speed = the speed of the object
local function MoveTowardsPlayer( player, shape, speed )
    local velocity = {}
    velocity.x = (player.shape.x - shape.x)
    velocity.y = (player.shape.y - shape.y) 

    local mag = math.sqrt( (velocity.x*velocity.x) + (velocity.y*velocity.y) )
    velocity.x = (velocity.x/mag)* speed
    velocity.y = (velocity.y/mag)* speed

    shape:applyForce(velocity.x, velocity.y, shape.x, shape.y )
end

--> Create an explosion particle effect at the given location
local function ExplosionParticles( xLoc, yLoc, parentGroup )
    for i=1, 30 do
        local particle = display.newCircle( parentGroup, xLoc, yLoc, 2 )
        physics.addBody(particle, "dynamic", { radius = 2, bounce = 2, density = 1, isSensor = true})

        particle:applyForce( math.random( -2,3 )-math.random( ), math.random( -2,3 )-math.random( ), particle.x, particle.y )
        transition.fadeOut( particle, {time = 1000, onComplete = function ( )
            if particle then
                display.remove( particle )
                particle = nil
            end
        end} )
    end

    globals.playSFX(_G.soundTable.spawn)
end
----- </LOCAL FUNCTIONS> -----

--> Base Projectile Class
local Projectile = {}
Projectile.__index = Projectile

setmetatable( Projectile, {
        __call = function (cls, ...)
        local self = setmetatable({}, cls)
        self:Initialize(...)
        return self
        end,
    } )

--> Base Initialize function
function Projectile:Initialize( init )
    if init.player and init.parentGroup then
        local shape = globals.newImageRectNoDimensions(init.fileName)
        init.parentGroup:insert( shape )

        shape.collision = Projectile_Collision
        shape:addEventListener( "collision" )

        self.shape = shape
        shape.base = self
    else
        return false
    end

    self.name = init.name
    self.isDead = false

    self.isPointed = false
    self.comingFrom = "none" -- Which side of the screen this projectile is coming from
    self.isExplosionDone = false -- Whether this spawn has done its explosion effect already

    return true
end

--> Call this when the projectile is about to be destroyed
function Projectile:destroy(  )
    self.shape:removeEventListener( "collision" )
end

--> Base Update function
-- Must be called at a game loop with the reference to the player as one of the parameter
function Projectile:update( player, particleGroup )
    if self.isDead then
        local velocityX, velocityY = -(player.shape.x - self.shape.x), -(player.shape.x - self.shape.x)
        local mag = math.sqrt( (velocityX*velocityX) + (velocityY*velocityY) )
        velocityX = (velocityX/mag) * 0.1
        velocityY = (velocityY/mag) * 0.1
        self.shape:applyForce( velocityX, velocityY, self.shape.x, self.shape.y )
    elseif not self.isExplosionDone then
        if self.comingFrom == "top" and self.shape.y>= globals.points.top then
            ExplosionParticles( self.shape.x, globals.points.top, particleGroup )
            self.isExplosionDone = true
        elseif self.comingFrom == "bottom" and self.shape.y<=globals.points.bottom then
            ExplosionParticles( self.shape.x, globals.points.bottom, particleGroup )
            self.isExplosionDone = true
        elseif self.comingFrom == "left" and self.shape.x>=globals.points.left then
            ExplosionParticles( globals.points.left, self.shape.y, particleGroup )
            self.isExplosionDone = true
        elseif self.comingFrom == "right" and self.shape.x<=globals.points.right then
            ExplosionParticles( globals.points.right, self.shape.y, particleGroup )
            self.isExplosionDone = true
        end
    end
end

local Wall = {}
for k, v in pairs(Projectile) do
  Wall[k] = v
end
Wall.__index = Wall

setmetatable(Wall, {
  __index = Projectile, -- this is what makes the inheritance work
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:Initialize(...)
    return self
  end,
})

function Wall:Initialize( init )
    local wall = display.newRect(init.parentGroup, 0, 0, init.width, init.height)
    wall:setFillColor( 0 )

    -- Right
    if init.direction == 1 then
        wall.x = init.player.shape.x + init.distance
        wall.y = globals.points.top - 50

        physics.addBody( wall, "kinematic" )
        wall:setLinearVelocity( 0, init.speed)
    -- Left
    elseif init.direction == 2 then
        wall.x = init.player.shape.x - init.distance
        wall.y = globals.points.top - 50

        physics.addBody( wall, "kinematic" )
        wall:setLinearVelocity( 0, init.speed)
    -- Up
    elseif init.direction == 3 then
        wall.width, wall.height = 100, 20
        wall.x = globals.points.left - 50
        wall.y = globals.points.centerY - 150

        physics.addBody( wall, "kinematic" )
        wall:setLinearVelocity( init.speed, 0)
    -- Down
    elseif init.direction == 4 then
        wall.width, wall.height = 100, 20
        wall.x = globals.points.left - 50
        wall.y = globals.points.centerY + 150

        physics.addBody( wall, "kinematic" )
        wall:setLinearVelocity( init.speed, 0)
    end

    self.shape = wall
    wall.base = self
end

--> Needs to be overriden with a blank
-- Don't remove this
function Wall:destroy( )

end

local function Wall_Spawn( parentGroup, player )
    local randDir = math.random( 2 )

    return Wall({
        parentGroup = parentGroup,
        player = player,
        name = "wall",
        speed = 60,
        distance = 100,
        width = 20,
        height = 100,
        direction = randDir
    })
end

local Duo = {}
for k, v in pairs(Projectile) do
  Duo[k] = v
end
Duo.__index = Duo

setmetatable(Duo, {
  __index = Projectile, -- this is what makes the inheritance work
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:Initialize(...)
    return self
  end,
})

function Duo:Initialize( init )
    if not Projectile.Initialize(self, init) then return end

    self.shape.y = init.yPos
    self.speed = init.speed

    if init.index == 1 then
        self.shape.x = globals.points.left - 20
        self.comingFrom = "left"
    else
        self.shape.x = globals.points.right + 20
        self.comingFrom = "right"
    end

    local triangleShape = { 0,-15, 20,16, -20,16 }

    physics.addBody(self.shape, "dynamic", { shape = triangleShape, bounce = 1, density = 0})

    MoveTowardsPlayer(init.player, self.shape, init.speed)
end

local function Duo_Spawn( parentGroup, player, waveCount, currentLevel )
    local fileNames = {
        "assets/img/game/enemy/t1.png",
        "assets/img/game/enemy/t2.png",
        "assets/img/game/enemy/t3.png"
    }

    local duoTable = {}
    local speed = math.random(2) - math.random()
    local yPos = math.random( display.contentHeight )

    speed = speed + waveCount/2

    if speed > 10 then speed = 10 end -- Limit max speed

    for i=1, 2 do
        duoTable[#duoTable+1] = Duo({
        parentGroup = parentGroup,
        player = player,
        name = "duo",
        fileName = fileNames[math.random(#fileNames)],
        speed = speed,
        yPos = yPos,
        index = i,
    })
    end

    if currentLevel >=3 then currentLevel = 3 end
    duoTable.delay = 2000 / currentLevel
    duoTable.delay = duoTable.delay - math.random(500,1000)
    if duoTable.delay < 500 then duoTable.delay = 500 + math.random(500,1000) end

    return duoTable
end

local Line = {}
for k, v in pairs(Projectile) do
  Line[k] = v
end
Line.__index = Line

setmetatable(Line, {
  __index = Projectile, -- this is what makes the inheritance work
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:Initialize(...)
    return self
  end,
})

function Line:Initialize( init )
    if not Projectile.Initialize(self, init) then return end

    physics.addBody(self.shape, "dynamic", { bounce = 3, density = 0})

    local distance = 80 - (init.speed * 2)
    -- Top
    if init.direction == 1 then
        self.shape.x = init.randX
        self.shape.y = globals.points.top - (distance * init.index)
        self.comingFrom = "top"
    -- Bot
    elseif init.direction == 2 then
        self.shape.x = init.randX
        self.shape.y = globals.points.bottom + (distance * init.index)
        self.comingFrom = "bottom"
    -- Left
    elseif init.direction == 3 then
        self.shape.x = globals.points.left - (distance * init.index)
        self.shape.y = init.randY
        self.comingFrom = "left"
    -- Right
    else
        self.shape.x = globals.points.right + (distance * init.index)
        self.shape.y = init.randY
        self.comingFrom = "right"
    end

    self.shape.collision = Projectile_Collision
    MoveTowardsPlayer(init.player, self.shape, init.speed)
    self.shape:applyTorque(1)
end

local function Line_Spawn( parentGroup, player, waveCount, currentLevel )
    local fileNames = {
        "assets/img/game/enemy/s1.png",
        "assets/img/game/enemy/s2.png",
        "assets/img/game/enemy/s3.png"
    }

    local lineTable = {}
    local speed = 1 + math.random()
    local randDir = math.random( 2 )
    local randomX = math.random( display.contentWidth )
    local randomY = math.random( display.contentHeight )

    speed = speed + waveCount/1.5

    if speed > 10 then speed = 10 end -- Limit max speed

    for i=1,5 do
        lineTable[#lineTable+1] = Line({
        parentGroup = parentGroup,
        player = player,
        name = "line",
        index = i,
        speed = speed,
        fileName = fileNames[math.random(#fileNames)],
        direction = randDir,
        randX = randomX,
        randY = randomY,
        })
    end
    lineTable.delay = 7000 - (500 * speed)
    lineTable.delay = lineTable.delay - math.random(500,1000)
    if lineTable.delay < 2000 then lineTable.delay = 2000 + math.random(500,1000) end

    return lineTable
end

local PaperClip = {}
for k, v in pairs(Projectile) do
  PaperClip[k] = v
end
PaperClip.__index = PaperClip

setmetatable(PaperClip, {
  __index = Projectile, -- this is what makes the inheritance work
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:Initialize(...)
    return self
  end,
})

function PaperClip:Initialize( init )
    if init.pattern[init.index] == 0 then return end
    
    if not Projectile.Initialize(self, init) then return end -- Call base initialize function

    self.shape:scale( 0.5, 0.5 )

    local offsetRectParams = { halfWidth=10, halfHeight=10}

    physics.addBody(self.shape, "dynamic", { box = offsetRectParams, bounce = 3, density = 0})
    self.shape.collision = Projectile_Collision

    local distance = 50
    self.speed = init.speed
    self.isEarly = init.isEarly

    self.state = "none"
    if init.direction == 1 then
        self.startState = "topLeft"
        self.comingFrom = "top"
        self.shape.x = globals.points.centerX - 100
        self.shape.y = globals.points.top - (distance * init.index)
        self.shape:applyForce(0, self.speed, self.shape.x, self.shape.y )
    elseif init.direction == 2 then
        self.startState = "topRight"
        self.comingFrom = "top"
        self.shape.x = globals.points.centerX + 100
        self.shape.y = globals.points.top - (distance * init.index)
        self.shape:applyForce(0, self.speed, self.shape.x, self.shape.y )
    elseif init.direction == 3 then
        self.startState = "botLeft"
        self.comingFrom = "bottom"
        self.shape.x = globals.points.centerX - 100
        self.shape.y = globals.points.bottom + (distance * init.index)
        self.shape:applyForce(0, -self.speed, self.shape.x, self.shape.y )
    elseif init.direction == 4 then
        self.startState = "botRight"
        self.comingFrom = "bottom"
        self.shape.x = globals.points.centerX + 100
        self.shape.y = globals.points.bottom + (distance * init.index)
        self.shape:applyForce(0, -self.speed, self.shape.x, self.shape.y )
    end  
end


-- I don't even know how I made this work.
--> Moves the paperclip object around the player
function PaperClip:update( player, particleGroup )
    Projectile.update(self, player, particleGroup) -- Call base update function

    if self.startState == "topLeft" and not self.isDead and self.shape.y >= globals.points.bottom - 50 then
        self.state = "right"
    elseif self.startState == "topRight" and not self.isDead and self.shape.y >= globals.points.bottom - 50 then
        self.state = "left"
    elseif self.startState == "botLeft" and not self.isDead and self.shape.y <= globals.points.top + 50 then
        self.state = "right"
    elseif self.startState == "botRight" and not self.isDead and self.shape.y <= globals.points.top + 50 then
        self.state = "left"
    end

    if self.state == "right" and ( (not self.isEarly and self.shape.x >= globals.points.right - 50) or (self.isEarly and self.shape.x >= player.shape.x) ) then
        if self.startState == "topLeft" then
            self.state = "up"
        elseif self.startState == "botLeft" then
            self.state = "down"
        end
    end

    if self.state == "left" and ( (not self.isEarly and self.shape.x <= globals.points.left + 50) or (self.isEarly and self.shape.x <= player.shape.x) ) then
        if self.startState == "topRight" then
            self.state = "up"
        elseif self.startState == "botRight" then
            self.state = "down"  
        end
    end

    if self.state == "up" and self.shape.y <= globals.points.top + 70 then
        if self.startState == "topRight" then
            self.state = "right"
        elseif self.startState == "topLeft" then
            self.state = "left"
        end
    end

    if self.state == "down" and self.shape.y >= globals.points.bottom - 70 then
        if self.startState == "botLeft" then
            self.state = "left"
        elseif self.startState == "botRight" then
            self.state = "right"
        end
    end

    if self.state == "leftCenter" and self.shape.x <= player.shape.x then
        if self.startState == "topLeft" then
            self.state = "down"
        elseif self.startState == "botLeft" then
            self.state = "up"
        end
    end
    if self.state == "rightCenter" and self.shape.x >= player.shape.x then
        if self.startState == "topRight" then
            self.state = "down"
        elseif self.startState == "botRight" then
            self.state = "up"
        end
    end

    -- Movement states
    if self.state == "right" then
        self.shape:setLinearVelocity( 0, 0 )
        self.shape:applyForce(self.speed, 0, self.shape.x, self.shape.y )

        if self.startState == "topRight" then
            self.state = "rightCenter"
        elseif self.startState == "botRight" then
            self.state = "rightCenter"
        end
    end

    if self.state == "left" then
        self.shape:setLinearVelocity( 0, 0 )
        self.shape:applyForce(-self.speed, 0, self.shape.x, self.shape.y )

        if self.startState == "topLeft" then
            self.state = "leftCenter"
        elseif self.startState == "botLeft" then
            self.state = "leftCenter"
        end
    end

    if self.state == "up" then
        self.shape:setLinearVelocity( 0, 0 )
        self.shape:applyForce(0, -self.speed, self.shape.x, self.shape.y )

        if self.startState == "botLeft" or self.startState == "botRight" then
            self.state = "none"
        end
        if self.isEarly and (self.startState == "topLeft" or self.startState == "topRight") then
            self.state = "none"
        end
    end

    if self.state == "down" then
        self.shape:setLinearVelocity( 0, 0 )
        self.shape:applyForce(0, self.speed, self.shape.x, self.shape.y )

        if self.startState == "topLeft" or self.startState == "topRight" then
            self.state = "none"
        end
        if self.isEarly and (self.startState == "botLeft" or self.startState == "botRight") then
            self.state = "none"
        end
    end
end

local function PaperClip_Spawn( parentGroup, player, waveCount, currentLevel )
    local fileNames = {
        "assets/img/game/enemy/Obstacle_1.png",
        "assets/img/game/enemy/Obstacle_2.png",
        "assets/img/game/enemy/Obstacle_3.png",
        "assets/img/game/enemy/Obstacle_4.png",
        "assets/img/game/enemy/Obstacle_5.png"
    }
    local paperClipTable = {}
    local speed = 1 + math.random()
    local random = math.random( 4 )
    local pattern = {
        { 1, 1, 1, 1, 1, 1 },
        { 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1},
        { 1, 1, 0, 1, 1, 1, 1, 1}
    }
    local randPattern = math.random( #pattern )

    -- Whether this spawn wave will turn towards the player on the first turn
    local isEarly = (math.random(2) == 1)

    speed = speed + waveCount/6
    if speed > 2.5 then speed = 2.5 end -- Limit max speed

    for i=1,#pattern[randPattern] do
        paperClipTable[#paperClipTable+1] = PaperClip({
            parentGroup = parentGroup,
            player = player,
            fileName = fileNames[math.random(#fileNames)],
            name = "paperclip",
            index = i,
            speed = speed,
            direction = random,
            pattern = pattern[randPattern],
            isEarly = isEarly
        })
    end

    if isEarly then
        paperClipTable.delay = 5000 - (1000 * (speed/#pattern[randPattern]) )
    else
        paperClipTable.delay = 10000 - (1500 * (speed/#pattern[randPattern]) )
    end
    return paperClipTable
end

local function RandomSpawn( spawnTable, parentGroup, player, waveCount, currentLevel)
    local spawnFunc = spawnTable[math.random(#spawnTable)] 

    return spawnFunc(parentGroup, player, waveCount, currentLevel)
end

M.Wall = Wall_Spawn
M.Duo = Duo_Spawn
M.Line = Line_Spawn
M.PaperClip = PaperClip_Spawn

-- Calls a random spawn function from the table given
M.Random = RandomSpawn

return M