* All code lines are written like this / local x = 0 /

I. About player.lua
	Similar to spawn.lua (albeit simpler), this module uses OOP and classes using metatables, so go read about that if you want to learn more about it
	I used the code from this link: http://lua-users.org/wiki/ObjectOrientationTutorial

	Calling this module will return an instance of the player, but does not appear yet on the screen:
	   / local player = require( "player" ) /

	To create the player image and initialize its values:
	    / player:create(...) /

	You can't do anything to the player until it is created and initialized
	Don't forget to call / player:destroy() / when exiting the scene. 
	    * Doesn't actually destroy the player when you call this (LOL), just removes listeners. I'll implement that soon maybe....

	The returned player instance has the following properties (after calling player:create):
		1. name (string) - the unique identifier for this object. Might be used later on to differentiate different types of player classes or something
		2. isDead (bool) - is this player currently dead?
		3. isAttacking (bool) - is the player currently attacking/deflecting? Aka is the aura/pulse currently present and active
		4. canAttack (bool) - can the player currently attack/deflect? Used to prevent spamming attack/deflect
		5. auraSize (number) - the current size of the expanding collision shape
		6. auraTime (number) - the aura/pulse uptime
		7. auraShape (table/object) - the reference to the deflect shape/collision box (not the player's collision box). Is nil if the player is not currently deflecting
		8. auraMaxSize (number) - the max size of the aura. If reached, the auraShape will be destroyed
		9. deflections (number) - how many enemy deflections has the player done?
		10. skinIndex (number) - the index of the skin the player is currently equipped
		11. trail (sprite) - reference to the trail sprite below the player
		12. shape (table/object) - the reference to the image
                    Note: this means that self and shape are two DIFFERENT things
                    "self" refers to the instance of the class and contains the values such as the name, etc
                    "shape" is what collision returns as event.other, it is the object you should apply physics to
                    	you can access it on its functions by calling / self.shape /, on game.lua, / player.shape /
                    	self.shape also has the position and other data of the display object
		                you can also access the self from the shape by calling / shape.base /

    player.shape also has the following events attached to it:
		"aura" - fires when an enemy hits the player's deflect/aura/pulse
		"playerCollision" - fires when an enemy hits the player itself (called regardless if the player is dead or not)
		"playerDie" - fires once when the player is first hit by an enemy
	To use: / player.shape:addEventListener( "aura", Player_Aura ) /

	If you want to add more properties, add them to the PlayerData table
	Player also has methods/functions which are identified as Player:<method/function name>
	They are explained in comment when you read them

II. About spawn.lua
	Similar to player.lua, this module uses OOP and inheritance using metatables, so go read about that if you want to learn more about it
    I used the code from this link: http://lua-users.org/wiki/ObjectOrientationTutorial

    This makes creating new types of enemies and spawn waves easier and without any need of copy-pasting the same code
   
    Basically all spawned enemies inherit from one projectile class, so whatever this base class does, the inherited classes does also
    This is especially useful since all enemies spawn using the same code, so I don't have to duplicate the code

    Here are the basic properties of all projectiles/enemies:
    1. name (string) - the unique identifier for this spawned enemy, use this if you want to identify what enemy it is
    2. isDead (bool) - is true when the player has already deflected this enemy
    3. isExplosionDone (bool) - is true when the spawn explosion particle effect has already happened. Basically to ensure the particle effect happens once
    4. isPointed (bool) - used by the player to check if this enemy has been deflected and ensures that player.deflections count doesn't count the enemy twice when incrementing
    5. speed (number) - the current speed of this enemy
    6. comingFrom (string) - which side of the screen this enemy is coming from. You should be the one to set this when you initialize this object. Is needed for the explosion particle effect
    		use: "top", "bottom", "left", "right", "none"
    7. shape (table/object) - same as the one on the player's. The reference to the spawned image
    		acess as / self.shape / on its functions or / spawn.shape / on game.lua
    
    Here are the basic methods/functions of all projectiles:
    1. update - the update function of this object. This MUST be manually called in a game loop or something else it won't work
    2. initiazlize - is called when the class name is called (possible thanks to _call in metatables). Initializes and spawns the enemy
    3. destroy - called when the enemy is removed from display

    Some types of enemies has their own unique properties, like PaperClip has a special property "state" that is used when it is moving around the player
    You can add properties like this when needed
    Special functions can also be made and is unique only for that enemy

    To create a new type of spawn wave:
    Just copy paste this code below the latest spawn wave, replace <name> with the name of the enemy class

    /
	    local <name> = {}
		for k, v in pairs(Projectile) do
		  <name>[k] = v
		end
		<name>.__index = <name>

		setmetatable(<name>, {
		  __index = Projectile, -- this is what makes the inheritance work
		  __call = function (cls, ...)
		    local self = setmetatable({}, cls)
		    self:Initialize(...)
		    return self
		  end,
		})

		function <name>:Initialize( init )
		    if not Projectile.Initialize(self, init) then return end -- Call base initialize function

		    -- Base function only spawns the enemy, its your job to make it move
		    -- physics.addBody should also be added here: 

		    -- physics.addBody(self.shape, "dynamic")
	    	-- self.shape.collision = Projectile_Collision
		end

		-- This code can be ommitted when you're not gonna add anything
		function <name>:destroy( )
			Projectile.destroy(self)
		end

		-- This code can be ommitted when you're not gonna add anything
		function <name>:update( player, particleGroup )
			Projectile.Update(self, player, particleGroup) -- Call base update function
		end

		local function <name>_Spawn( parentGroup, player, waveCount, currentLevel )
		    return <name>({
		        parentGroup = parentGroup,
		        player = player,
		        name = "",
		        fileName = ""
		    })
		end
	/

	Add this also to the list of module function at the end of the code
	M.<name> = <name>_Spawn

III. How to add new player skins
	On main.lua:
	/
		playerSkinData = {
			...
			...
				skinType = "premium"
			},
			-- Add a new array element here
			-- skin #
			{
				avatarFile = "",
				deflectFile = "",
				upFile = "",
				downFile = "",
				colliderShape = "circle",
				skinType = "premium"
			}
			-- end
		}
	/
	This is all you need to do. The skin is automatically added to the respective scrollview on the skins popup scene
	When that skin is selected, the title screen and the game will use that skin

IV. How to add new gradient backgrounds
	On main.lua:

	/
		gradientColors = {
			...
			...
			_fcb69f = {0.9882,0.7137,0.6235},

			-- Add the new gradient colors here
			_<hexcode1> = {r,g,b}
			_<hexcode2> = {r,g,b}
		}
	/

	Also on main.lua:
	/
		gradientList = {
			....
			....
			{
				{
					type = "gradient",
				    color1 = gradientColors._fcb69f,
				    color2 = gradientColors._ffecd2
			    }
			}
			-- Add new gradients here
			-- Level #
			{
				{
					type = "gradient",
				    color1 = gradientColors._<hexcode1>,
				    color2 = gradientColors._<hexcode2>
				},
				{
					type = "gradient",
				    color1 = gradientColors._<hexcode2>,
				    color2 = gradientColors._<hexcode1>
				}
			}
			-- end
		}
	/