---------------------------------------------------------------------------------------------------------------
--[[ 
main.lua
	Called when starting the app
	Contains global variables and other initializations
	Check out FAQ.txt for more information about the code in this game (the ones that are too long to be included as comments :3)
--]]
---------------------------------------------------------------------------------------------------------------

----- <SCENE REQUIREMENTS> -----
local composer = require "composer"
local globals = require( "globals" )
firebaseAnalytics = require "plugin.firebaseAnalytics"
admob = require( "plugin.admob" )
save = require("save")
----- </SCENE REQUIREMENTS> -----

----- <OBJECTS> -----

----- </OBJECTS> -----

----- <VARIABLES> -----
local launchArgs = ...

whichPlatform = system.getInfo( "platformName" )    --> Used to check for platforms

gameTitle = "Hypercasual Game"
buildNumber = "0.0.7"
----- </VARIABLES> -----

----- <INITIALIZATIONS> -----
-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

--firebaseAnalytics.init() --> Init Firebase 
----- </INITIALIZATIONS> -----

----- <FUNCTIONS> -----

----- </FUNCTIONS> -----
------------------------------------------------------------
-- Useful Globals
------------------------------------------------------------
----- <AUDIO> -----
-- Reserves the first 5 channels for music use only. 
-- The usage of each channel is as follows:
-- 1 = music for the title screen
-- 2 & 3 = music for the game. Two channels to allow for crossfading between two bgm when transitioning to a new background
-- 4-5 = spare channels. Use when needed
audio.reserveChannels( 5 )

--> Loads the music for the game
function loadStreamTable(  )
	streamTable = {
		bgm1 = audio.loadStream("assets/audio/BGM001 [Park Bench].mp3"),
		bgm2 = audio.loadStream("assets/audio/BGM002 [Paris].mp3"),
		bgm3 = audio.loadStream("assets/audio/BGM003 [Follow Her].mp3"),
		titleScreen = audio.loadStream("assets/audio/GameOver [Mistakes].mp3")
	}
end

--> Loads the sound effects
-- Used in conjunction with globals.playSFX
function loadSoundTable()
	soundTable = {
		button = {
			audio.loadSound( "assets/audio/Button.mp3" )
		},
		getCoin = {
			audio.loadSound( "assets/audio/Get Coin.mp3" )
		},
		playerDie = {
			audio.loadSound( "assets/audio/Hit001.mp3" ),
			audio.loadSound( "assets/audio/Hit002.mp3" ),
			audio.loadSound( "assets/audio/Hit003.mp3" )
		},
		pause = {
			audio.loadSound( "assets/audio/Pause.mp3" )
		},
		-- When the player deflects an enemy
		deflect = {
			audio.loadSound( "assets/audio/deflect.mp3" )
		},
		-- When the player sends out a pulse
		attack = {
			audio.loadSound( "assets/audio/deflect_wave.mp3" )
		},
		-- Enemy hits another enemy
		bump = {
			audio.loadSound( "assets/audio/bump.mp3" )
		},
		spawn = {
			audio.loadSound( "assets/audio/enter01.mp3" ),
			audio.loadSound( "assets/audio/enter02.mp3" ),
			audio.loadSound( "assets/audio/enter03.mp3" )
		}
	}
end

-- Disposes all audio from the sound table
function disposeSoundTable( )
	if _G.soundTable then
		for k,v in pairs( _G.soundTable ) do
			for i=1, #_G.soundTable[k] do
				audio.dispose( _G.soundTable[k][i] )
			end
			_G.soundTable[k] = nil
		end
	end
end

-- Disposes all audio from the stream table
function disposeStreamTable( )
	if _G.streamTable then
		for k,v in pairs( _G.streamTable ) do
			audio.dispose( _G.streamTable[k] )
			_G.streamTable[k] = nil
		end
	end
end

--> If sound setting is off, mute all sound channels
--> If music setting is off, mute all music channels
--> Else set channel's volume to 1 (unmutes the channel)
function updateAudioPerSetting(  )
	local isSoundOn = _G.save.loadData().isSoundOn
	local isMusicOn = _G.save.loadData().isMusicOn

	local volume = 0
    if isSoundOn then volume = 1 end

    for i=audio.reserveChannels(), audio.unreservedFreeChannels do
        audio.setVolume( volume, {channel = i} )
    end

    local volume = 0
    if isMusicOn then volume = 1 end

    for i=1, audio.reserveChannels() do
        audio.setVolume( volume, {channel = i} )
    end
end

--> Fades out the given audio channel
-- Only fades out if the music setting is on
-- channel (number) = the number of the channel to fade out
-- time (number) = in ms. The fadeout time
function musicFadeOut( channel, time )
	local isMusicOn = _G.save.loadData().isMusicOn

	if isMusicOn then
		audio.fade( { channel=channel, time=time, volume=0} )
	end
end

--> Fades in the given audio channel
-- Only fades in if the music setting is on
-- channel (number) = the number of the channel to fade in
-- time (number) = in ms. The fadein time
function musicFadeIn( channel, time )
	local isMusicOn = _G.save.loadData().isMusicOn

	if isMusicOn then
		audio.fade( { channel=channel, time=time, volume=1} )
	end
end
----- </AUDIO> -----

----- <FONTS> -----
--> Global table of fonts
-- Used on display.newText
-- to use: _G.fontTable.Regular
fontTable = {
	Bold = "assets/fonts/JosefinSans-Bold.ttf",
	BoldItalic = "assets/fonts/JosefinSans-BoldItalic.ttf",
	Italic = "assets/fonts/JosefinSans-Italic.ttf",
	Light = "assets/fonts/JosefinSans-Light.ttf",
	LightItalic = "assets/fonts/JosefinSans-LightItalic.ttf",
	Regular = "assets/fonts/JosefinSans-Regular.ttf",
	SemiBold = "assets/fonts/JosefinSans-SemiBold.ttf",
	SemiBoldItalic = "assets/fonts/JosefinSans-SemiBoldItalic.ttf",
	Thin = "assets/fonts/JosefinSans-Thin.ttf",
	ThinItalic = "assets/fonts/JosefinSans-ThinItalic.ttf"
}
----- </FONTS> -----

----- <PLAYER> -----
--> Contains data of each of the player's skins
-- This is what the current skin index references to
-- This table/array should be read only else some bugs may occur
-- playerSkinData[index] returns a table of information about the skin
-- avatarFile = the filename of the avatar image
-- deflectFile = the filename of the deflect aura/pulse
-- upFile & downFile = used in the skins popup scene. Filenames of the buttons' default and over files, respectively
-- colliderShape = the shape of the deflect aura/pulse collision. Currently, there are only two:
-- 			"box" = uses a box that's rotated 30 degrees
-- 			"circle" = default shape if colliderShape is not specified or is not valid
-- skinType = use only "regular" or "premium". This determines which tab in the skin popup this skin will belong to
playerSkinData = {
	-- Skin 1
	{
		avatarFile = "assets/img/game/player/Avatar1.png",
		deflectFile = "assets/img/game/player/circleDeflect.png",
		upFile = "assets/img/menu/skins/btn_Skin1Up.png",
		downFile = "assets/img/menu/skins/btn_Skin1Down.png",
		colliderShape = "circle",
		skinType = "regular"
	},
	-- Skin 2
	{
		avatarFile = "assets/img/game/player/Avatar2.png",
		deflectFile = "assets/img/game/player/circleDeflect.png",
		upFile = "assets/img/menu/skins/btn_Skin2Up.png",
		downFile = "assets/img/menu/skins/btn_Skin2Down.png",
		colliderShape = "circle",
		skinType = "premium"
	},
	-- Skin 3
	{
		avatarFile = "assets/img/game/player/Avatar3.png",
		deflectFile = "assets/img/game/player/boxDeflect.png",
		upFile = "assets/img/menu/skins/btn_Skin3Up.png",
		downFile = "assets/img/menu/skins/btn_Skin3Down.png",
		colliderShape = "box",
		skinType = "regular"
	},
	-- Skin 4
	{
		avatarFile = "assets/img/game/player/Avatar4.png",
		deflectFile = "assets/img/game/player/hexagonDeflect.png",
		upFile = "assets/img/menu/skins/btn_Skin4Up.png",
		downFile = "assets/img/menu/skins/btn_Skin4Down.png",
		colliderShape = "circle",
		skinType = "regular"
	},
	-- Skin 5
	{
		avatarFile = "assets/img/game/player/Avatar6.png",
		deflectFile = "assets/img/game/player/boxDeflect.png",
		upFile = "assets/img/menu/skins/btn_Skin6Up.png",
		downFile = "assets/img/menu/skins/btn_Skin6Down.png",
		colliderShape = "box",
		skinType = "premium"
	},
	-- Skin 6
	{
		avatarFile = "assets/img/game/player/Avatar5.png",
		deflectFile = "assets/img/game/player/hexagonDeflect2.png",
		upFile = "assets/img/menu/skins/btn_Skin5Up.png",
		downFile = "assets/img/menu/skins/btn_Skin5Down.png",
		colliderShape = "circle",
		skinType = "premium"
	}
}
----- <PLAYER> -----
----- <DISPLAY> -----
--> Table of color values used for the gradients
-- I used the color's hex code as the table key for easier indetification
gradientColors = {
	_03ddfd = {0.0117,0.8666,0.9921},
	_d3f78e = {0.8274,0.9686,0.5568},

	_fc5c7d = {0.9882,0.3607,0.4901},
	_6a82fb = {0.4156,0.5098,0.9843},

	_eea849 = {0.9333,0.6588,0.2862},
	_f46b45 = {0.9568,0.4196,0.2705},

	_649173 = {0.3921,0.5686,0.4509},
	_dbd5a4 = {0.8588,0.8352,0.6431},

	_dae2f8 = {0.8549,0.8862,0.9725},
	_d6a4a4 = {0.8392,0.6431,0.6431},

	_ffd194 = {1,0.8196,0.5803},
	_70e1f5 = {0.4392,0.8823,0.9607},
--
	_fa709a = {0.9803,0.4392,0.6039},
	_fee140 = {0.9960,0.8823,0.2509},

	_a8edea = {0.6588,0.9294,0.9176},
	_fed6e3 = {0.9960,0.8392,0.8901},

	_0250c5 = {0.0078,0.3137,0.7725},
	_d43f8d = {0.8313,0.2470,0.5529},

	_93a5cf = {0.5764,0.6470,0.8117},
	_e4efe9 = {0.8941,0.9372,0.9137},

	_50cc7f = {0.3137,0.8,0.4980},
	_f5d100 = {0.9607,0.8196,0},

	_ffecd2 = {1,0.9254,0.8235},
	_fcb69f = {0.9882,0.7137,0.6235}
}

--> The gradient data for the backgrounds for each level
-- To use: bg.fill = _G.gradientList[levelNumber][colorIndex]
-- where: levelNumber = the number of the current level or index
-- 		  colorIndex = either 1 or 2. Used in alternation for seamless backgrounds
-- IMPORTANT: Odd numbered levels are bright-colored gradients while even numbered levels are desaturated-colored gradients
gradientList = {
	-- Level 1
	{
		{
			type = "gradient",
		    color1 = gradientColors._03ddfd,
		    color2 = gradientColors._d3f78e
		},
		{
			type = "gradient",
		    color1 = gradientColors._d3f78e,
		    color2 = gradientColors._03ddfd
		}
	},
	-- Level 2
	{
		{
			type = "gradient",
		    color1 = gradientColors._649173,
		    color2 = gradientColors._dbd5a4
		},
		{
			type = "gradient",
		    color1 = gradientColors._dbd5a4,
		    color2 = gradientColors._649173
		}
	},
	-- Level 3
	{
		{
			type = "gradient",
		    color1 = gradientColors._fc5c7d,
		    color2 = gradientColors._6a82fb
		},
		{
			type = "gradient",
		    color1 = gradientColors._6a82fb,
		    color2 = gradientColors._fc5c7d
		}
	},
	-- Level 4
	{
		{
			type = "gradient",
		    color1 = gradientColors._dae2f8,
		    color2 = gradientColors._d6a4a4
		},
		{
			type = "gradient",
		    color1 = gradientColors._d6a4a4,
		    color2 = gradientColors._dae2f8
		}
	},
	-- Level 5
	{
		{
			type = "gradient",
		    color1 = gradientColors._eea849,
		    color2 = gradientColors._f46b45
		},
		{
			type = "gradient",
		    color1 = gradientColors._f46b45,
		    color2 = gradientColors._eea849
		}
	},
	-- Level 6
	{
		{
			type = "gradient",
		    color1 = gradientColors._ffd194,
		    color2 = gradientColors._70e1f5
		},
		{
			type = "gradient",
		    color1 = gradientColors._70e1f5,
		    color2 = gradientColors._ffd194
		}
	},
	-- Level 7
	{
		{
			type = "gradient",
		    color1 = gradientColors._fa709a,
		    color2 = gradientColors._fee140
		},
		{
			type = "gradient",
		    color1 = gradientColors._fee140,
		    color2 = gradientColors._fa709a
		}
	},
	-- Level 8
	{
		{
			type = "gradient",
		    color1 = gradientColors._a8edea,
		    color2 = gradientColors._fed6e3
		},
		{
			type = "gradient",
		    color1 = gradientColors._fed6e3,
		    color2 = gradientColors._a8edea
		}
	},
	-- Level 9
	{
		{
			type = "gradient",
		    color1 = gradientColors._0250c5,
		    color2 = gradientColors._d43f8d
		},
		{
			type = "gradient",
		    color1 = gradientColors._d43f8d,
		    color2 = gradientColors._0250c5
		}
	},
	-- Level 10
	{
		{
			type = "gradient",
		    color1 = gradientColors._93a5cf,
		    color2 = gradientColors._e4efe9
		},
		{
			type = "gradient",
		    color1 = gradientColors._e4efe9,
		    color2 = gradientColors._93a5cf
		}
	},
	-- Level 11
	{
		{
			type = "gradient",
		    color1 = gradientColors._50cc7f,
		    color2 = gradientColors._f5d100
		},
		{
			type = "gradient",
		    color1 = gradientColors._f5d100,
		    color2 = gradientColors._50cc7f
		}
	},
	-- Level 12
	{
		{
			type = "gradient",
		    color1 = gradientColors._ffecd2,
		    color2 = gradientColors._fcb69f
		},
		{
			type = "gradient",
		    color1 = gradientColors._fcb69f,
		    color2 = gradientColors._ffecd2
		}
	}
}

--> Creates the box borders inside the given background image
-- parentGroup (displayGroup) = the display group the border image will be inserted to
-- bg (image) = the background image the border will be created relative to
-- The border will automatically adjust based on the background image
function createBoxBorder( parentGroup, bg )
	local topLeft = globals.newImageRectNoDimensions("assets/img/menu/box/pTopLeft.png")
	topLeft.x, topLeft.y = globals.getLoc.left(bg) + 20, globals.getLoc.top(bg) + 20
	local topRight = globals.newImageRectNoDimensions("assets/img/menu/box/pTopRight.png")
	topRight.x, topRight.y = globals.getLoc.right(bg) - 20, globals.getLoc.top(bg) + 20

	local botLeft = globals.newImageRectNoDimensions("assets/img/menu/box/pTopBottomLeft.png")
	botLeft.x, botLeft.y = globals.getLoc.left(bg) + 20, globals.getLoc.bottom(bg) - 20
	local botRight = globals.newImageRectNoDimensions("assets/img/menu/box/pTopBottomRight.png")
	botRight.x, botRight.y = globals.getLoc.right(bg) - 20, globals.getLoc.bottom(bg) - 20

	local topBar = globals.newImageRectNoDimensions("assets/img/menu/box/pTopMiddle.png")
	topBar.x, topBar.y = globals.getLoc.centerX(bg), globals.getLoc.top(bg) + 20
	topBar.width = globals.getLoc.W(bg) - 80

	local botBar = globals.newImageRectNoDimensions("assets/img/menu/box/pBottomMiddle.png")
	botBar.x, botBar.y = globals.getLoc.centerX(bg), globals.getLoc.bottom(bg) - 20
	botBar.width = globals.getLoc.W(bg) - 80

	local leftBar = globals.newImageRectNoDimensions("assets/img/menu/box/pLeft.png")
	leftBar.x, leftBar.y = globals.getLoc.left(bg) + 19.7, globals.getLoc.centerY(bg)
	leftBar.height = globals.getLoc.H(bg) - 90

	local rightBar = globals.newImageRectNoDimensions("assets/img/menu/box/pRight.png")
	rightBar.x, rightBar.y = globals.getLoc.right(bg) - 19.7, globals.getLoc.centerY(bg)
	rightBar.height = globals.getLoc.H(bg) - 90

	parentGroup:insert(topLeft)
	parentGroup:insert(topRight)
	parentGroup:insert(botLeft)
	parentGroup:insert(botRight)

	parentGroup:insert(topBar)
	parentGroup:insert(botBar)
	parentGroup:insert(leftBar)
	parentGroup:insert(rightBar)
end
----- </DISPLAY> -----
------------------------------------------------------------
-- /Useful Globals
------------------------------------------------------------

----- <MAIN> -----
-- Initialize global variables and other stuff here

-- Produces a different sequence each time (assuming enough time between invocations)
math.randomseed( os.time() )

--> Shuffle the gradient list whenever the game is opened/started
-- Note: only shuffles bright colors (odd number) to bright colors
-- 			 and desaturated colors (even number) to desaturated colors
-- This also means that the title screen color will always be a bright color
for i = #gradientList, 1, -1 do
	local rand = math.random(#gradientList)

	if i%2 == 0 then
		while not (rand%2==0) do
			rand = math.random(#gradientList)
		end
	else
		while rand%2==0 do
			rand = math.random(#gradientList)
		end
	end
	gradientList[i], gradientList[rand] = gradientList[rand], gradientList[i]
end

--> Loads the sound and stream tables so you don't have to load them at the start of each scene
_G.loadSoundTable()
_G.loadStreamTable()

_G.updateAudioPerSetting()
----- </MAIN> -----

-- load menu screen
composer.gotoScene( "menu" )

-- Template code for doing rows and columns with only one for loop
--[[local row, column = 5, 1
for i=1, row*column do		
	i = i-1 -- arrays don't start with 0
	local iX = i - (column * math.floor( i/column ))
	local iY = math.floor( i/column )
end--]]