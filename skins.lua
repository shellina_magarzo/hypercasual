---------------------------------------------------------------------------------------------------------------
--[[ 
skins.lua (overlay scene)
	Popup scene for the settings/options
	Creates the scrollable regular and premium tabs/scrollviews
	Alternates between the regular and premium tabs/scrollviews
	Creates the skin buttons depending on the data specified in _G.playerSkinData
	Saves the equipped skin to persistent memory
	Updates the player avatar skin in the title screen once this scene is closed

	TODO: Locked skins and gacha
--]]
---------------------------------------------------------------------------------------------------------------
----- <SCENE REQUIREMENTS> -----
local composer = require( "composer" )
local globals = require( "globals" )
local scene = composer.newScene()
local widget = require("widget")
local displayGroups = {}
----- <SCENE REQUIREMENTS> -----

----- <OBJECTS> -----
local bg -- The backgound

-- Scroll views
local regularScrollView, premiumScrollView

-- Display groups
local mainGroup, regularGroup, premiumGroup

-- The button that says "regular" and "premium"
local regularBtn, premiumBtn = nil, nil

local skinButtons = {} -- A table of all the skin buttons

-- Check mark image
local checkMark
----- </OBJECTS> -----

----- <VARIABLES> -----
-- Function that fires once a button is pressed
local buttonEvent

local curSkin = _G.save.loadData().currentSkin -- The index of the current skin the player is wearing
local previousSkin = curSkin -- The index of skin that was previously selected

-- True if the current tab is on the regular scroll view
-- Shows the tab where the current equipped skin is on
local isOnRegularTab = _G.playerSkinData[curSkin].skinType == "regular"
----- </VARIABLES> -----

----- <INITIALIZATIONS> -----

----- </INITIALIZATIONS> -----

----- <FUNCTIONS> -----
--> Show the proper tab/scrollview depending on the isOnRegularTab bool
local function updateTabDisplay()
	local up, down = "Up","Down"
	if not isOnRegularTab then
		up, down = "Down", "Up"
	end

	if regularBtn then regularBtn:removeSelf( ) end
	if premiumBtn then premiumBtn:removeSelf( ) end
	
	regularBtn = globals.createImageButton( "regular",
				globals.getLoc.centerX(bg) - 47, globals.getLoc.centerY(bg),
				"assets/img/menu/skins/btn_Regular" .. down .. ".png", "assets/img/menu/skins/btn_Regular" .. up .. ".png",
				mainGroup, buttonEvent, 114/1.2, 45/1.2)
	premiumBtn = globals.createImageButton( "premium",
				globals.getLoc.centerX(bg) + 47, globals.getLoc.centerY(bg),
				"assets/img/menu/skins/btn_Premium" .. up .. ".png", "assets/img/menu/skins/btn_Premium" .. down .. ".png",
				mainGroup, buttonEvent, 114/1.2, 45/1.2)

	if isOnRegularTab then
		transition.fadeIn( regularGroup, {time = 200} )
		transition.fadeOut( premiumGroup, {time = 200} )
	else
		transition.fadeIn( premiumGroup, {time = 200} )
		transition.fadeOut( regularGroup, {time = 200} )
	end
end

--> Attaches the check mark to the button given the button's index
local function moveCheckMark( index )
	-- Delete old check mark
	if checkMark then checkMark:removeSelf( ) end

	-- Create the check mark
	checkMark = globals.newImageRectNoDimensions("assets/img/menu/skins/check.png")
    checkMark:scale( 0.7, 0.7 )

    local skinBtn = skinButtons[index]

    -- Move the check mark to the button
   	if _G.playerSkinData[index].skinType == "regular" then
		checkMark.x, checkMark.y = skinBtn.x+30, skinBtn.y-30
		regularScrollView:insert( checkMark )
   	else
		checkMark.x, checkMark.y = skinBtn.x+30, skinBtn.y-30
		premiumScrollView:insert( checkMark )
   	end
end

--> Changes the button image to the one with white border if selected
local function updateButtonImage( index )
	local curSkinBtn, prevSkinBtn = nil, nil

	local prevSkinGroup, curSkinGroup = nil, nil
	local prevScrollView, curScrollView = nil, nil

   	-- Change the previous skin button back to the one with black border
   	if _G.playerSkinData[previousSkin].skinType == "regular" then
		prevSkinGroup = regularGroup
		prevScrollView = regularScrollView
   	else
   		prevSkinGroup = premiumGroup
		prevScrollView = premiumScrollView
   	end

   	prevSkinBtn = globals.changeButtonImage(skinButtons[previousSkin], 
					_G.playerSkinData[previousSkin].upFile, _G.playerSkinData[previousSkin].downFile,
					prevSkinGroup, buttonEvent, true)
   	prevScrollView:insert(prevSkinBtn)

	-- Change the selected button's image to the one with the white border
   	if _G.playerSkinData[index].skinType == "regular" then
		curSkinGroup = regularGroup
		curScrollView = regularScrollView
   	else
		curSkinGroup = premiumGroup
		curScrollView = premiumScrollView
   	end
   	curSkinBtn = globals.changeButtonImage(skinButtons[index], 
					_G.playerSkinData[index].downFile, _G.playerSkinData[index].upFile,
					curSkinGroup, buttonEvent, true)
   	curScrollView:insert(curSkinBtn)

   	display.remove( skinButtons[index] )
   	display.remove( skinButtons[previousSkin] )

   	curSkinBtn.index = index
   	prevSkinBtn.index = previousSkin

   	skinButtons[index] = curSkinBtn
   	skinButtons[previousSkin] = prevSkinBtn

   	previousSkin = index
end

buttonEvent = function( event )
	local id = event.target.id

    if event.phase == "moved" then
    	local dy = math.abs( ( event.y - event.yStart ) )
        -- If the touch on the button has moved more than 10 pixels,
        -- pass focus back to the scroll view so it can continue scrolling
        if ( dy > 10 ) then
	    	if isOnRegularTab then regularScrollView:takeFocus( event )
	        else premiumScrollView:takeFocus( event ) end
	        return true
    	end
	elseif event.phase == "ended" then
		globals.playSFX(_G.soundTable.button)
		if id == "close" then
			composer.hideOverlay("crossFade", 500 )
		elseif id == "regular" then
			isOnRegularTab = true
			updateTabDisplay()
		elseif id == "premium" then
			isOnRegularTab = false
			updateTabDisplay()
		elseif id == "skin" then
			local index = event.target.index

			_G.save.savePlayerSkin(index)
			updateButtonImage(index)
			moveCheckMark(index)
		end
    end
    
end
----- </FUNCTIONS> -----

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
----- <CREATE> -----
function scene:create( event )
	local sceneGroup = self.view

	local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
	mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	regularGroup = globals.createDisplayGroup( "regularGroup", displayGroups, sceneGroup )
	premiumGroup = globals.createDisplayGroup( "premiumGroup", displayGroups, sceneGroup )
	local foreGroup = globals.createDisplayGroup( "foreGroup", displayGroups, sceneGroup )

	local rectOverlay = display.newRect( backGroup, globals.points.centerX, globals.points.centerY, globals.points.screenW, globals.points.screenH)
	rectOverlay:setFillColor( 0, 0.5 )

	bg = display.newRect( backGroup, globals.points.centerX, globals.points.centerY, 250, 450 )
	bg.fill = {
		type = "gradient",
	    color1 = { 0.5254,0.9921,0.9098 },
	    color2 = { 0.6745,0.7137,0.8980 }
	}

	_G.createBoxBorder(backGroup, bg)

	local titleText = display.newText( mainGroup, "SKINS", globals.getLoc.centerX(bg), globals.getLoc.top(bg) + 40, _G.fontTable.Bold, 25 )
	titleText:setFillColor( 0.2117 )

	local lineImg = globals.newImageRectNoDimensions("assets/img/menu/title/tDesign.png")
	lineImg.x, lineImg.y = titleText.x, titleText.y + 150
	mainGroup:insert( lineImg )

	local closeBtn = globals.createImageButton( "close",
				globals.getLoc.right(bg) - 20, globals.getLoc.top(bg) + 20,
				"assets/img/menu/options/btn_CloseUp.png", "assets/img/menu/options/btn_CloseDown.png",
				mainGroup, buttonEvent, 36, 36)

	-- Create the regular and premium scroll views
	regularScrollView = widget.newScrollView({
    	hideBackground = true,
    	horizontalScrollDisabled = true,
        top = globals.getLoc.centerY(bg)+20,
        left = globals.getLoc.left(bg) + 30,
        width = globals.getLoc.W(bg) - 60,
        height =  180,
		scrollHeight = 400,
        listener = scrollListener
    })
    regularGroup:insert( regularScrollView )

    premiumScrollView = widget.newScrollView({
    	hideBackground = true,
    	horizontalScrollDisabled = true,
        top = globals.getLoc.centerY(bg)+20,
        left = globals.getLoc.left(bg) + 30,
        width = globals.getLoc.W(bg) - 60,
        height =  180,
		scrollHeight = 400,
        listener = scrollListener
    })
    premiumGroup:insert( premiumScrollView )


    -- Creates and organizes the skin buttons based on _G.playeSkinData
    local premiumCount, regularCount = 0, 0
    for i=1, #_G.playerSkinData do
    	local data = _G.playerSkinData[i]
    	local skinBtn = nil

    	local upFile, downFile = data.upFile, data.downFile
    	if i == curSkin then upFile, downFile = downFile, upFile end

    	if data.skinType == "premium" then
    		local iX = premiumCount - (2 * math.floor( premiumCount/2 ))
			local iY = math.floor( premiumCount/2 )

    		skinBtn = globals.createImageButton( "skin", (iX*90)+50, (iY*85)+50,
					upFile, downFile,
					premiumGroup, buttonEvent, 114/1.3, 100/1.3, true)
    		premiumScrollView:insert( skinBtn )
    		premiumCount = premiumCount + 1
    	elseif data.skinType == "regular" then
    		local iX = regularCount - (2 * math.floor( regularCount/2 ))
			local iY = math.floor( regularCount/2 )

    		skinBtn = globals.createImageButton( "skin", (iX*90)+50, (iY*85)+50,
					upFile, downFile,
					regularGroup, buttonEvent, 114/1.3, 100/1.3, true)
    		regularScrollView:insert( skinBtn )
    		regularCount = regularCount + 1
    	end

    	skinBtn.index = i
    	table.insert( skinButtons, skinBtn )
    end

    -- Moves the check mark to the currently equipped skin
   	moveCheckMark(curSkin)
	updateTabDisplay()
end
----- </CREATE> -----

----- <SHOW> -----
function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then

	elseif phase == "did" then
	end	
end
----- </SHOW> -----
----- <HIDE> -----
function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	local parent = event.parent
	
	if event.phase == "will" then
		if parent then
			parent:updateSkin()
		end
	elseif phase == "did" then
		composer.removeScene("skins")
		
	end	
end
----- </HIDE> -----
----- <DESTROY> -----
function scene:destroy( event )
	local sceneGroup = self.view
	physics = nil
end
----- <DESTROY> -----
---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene